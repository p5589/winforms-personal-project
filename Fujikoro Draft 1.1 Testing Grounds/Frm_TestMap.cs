﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public partial class Frm_TestMap : Form
    {
        Player user = new Player("Model1");
        Mapspace[,] thismap = new Mapspace[15, 15];
        Mapspace thismapspace = new Mapspace();
        Player[] theseplayers;
        Dragon[] thesedragons;
        Scroll[] thesecrolls;
        int roundcounter = 1;

        public Frm_TestMap()
        {
            InitializeComponent();
        }
        private void Frm_TestMap_Load(object sender, EventArgs e)
        {
            int playercount = 0;
            int monkcount = 0;
            int scrollcount = 0;
            int dragoncount = 0;
            int resourcecount = 0;
            int emptycount = 0;
            string b = "Model2";
            string c = "Model3";
            Player opponent1 = new Player(b);
            Player opponent2 = new Player(c);
            Random generator = new Random();
            for (int i = 0; i < 14; i++)
            {
                for (int j = 0; j < 14; j++)
                {

                    Mapspace thismapspace = new Mapspace();
                    thismap[i, j] = thismapspace;
                    int diceroll = generator.Next(0, 66);
                    if ((playercount < 3) && (diceroll < 2))
                    {
                        thismapspace.ContainsPlayer = true;
                        switch (playercount)
                        {
                            case 0:

                                thismapspace.MapspacePlayer = user;
                                thismapspace.MapspacePlayer.PlayerpositionXaxis = i;
                                thismapspace.MapspacePlayer.PlayerpositionYaxis = j;
                                cmbPlayers.Items.Add(user);
                                break;
                            case 1:

                                thismapspace.MapspacePlayer = opponent1;
                                thismapspace.MapspacePlayer.PlayerpositionXaxis = i;
                                thismapspace.MapspacePlayer.PlayerpositionYaxis = j;
                                cmbPlayers.Items.Add(opponent1);
                                break;
                            case 2:
                                thismapspace.MapspacePlayer = opponent2;
                                thismapspace.MapspacePlayer.PlayerpositionXaxis = i;
                                thismapspace.MapspacePlayer.PlayerpositionYaxis = j;
                                cmbPlayers.Items.Add(opponent2);
                                break;
                            default:
                                break;
                        }
                        playercount++;


                    }
                    else if ((monkcount < 20) && (diceroll >= 3) && (diceroll < 10))
                    {
                        thismapspace.ContainsMonk = true;
                        thismapspace.ThisMonk = new Monk();
                        cmbMonks.Items.Add(thismapspace.ThisMonk);
                        monkcount++;

                    }
                    else if ((dragoncount < 20) && (diceroll >= 10) && (diceroll < 18))
                    {
                        thismapspace.ContainsDragon = true;
                        string model = $"Model {(generator.Next(0, 5).ToString())}";
                        int dice = generator.Next(0, 5);
                        int healthpoints = generator.Next(2, 8);
                        int initiative = generator.Next(3, 9);
                        thismapspace.MapspaceDragon = new Dragon(model, dice, healthpoints, initiative);
                        cmbDragons.Items.Add(thismapspace.MapspaceDragon);
                        dragoncount++;

                    }
                    else if ((scrollcount < 30) && (diceroll >= 18) && (diceroll < 28))
                    {
                        thismapspace.ThisScroll = new Scroll();
                        thismapspace.ContainsScroll = true;
                        thismapspace.ThisScroll.ScrollID = generator.Next(0, 11);
                        thismapspace.ThisScroll.ScrollText = $"Ik ben een scroll met ID {thismapspace.ThisScroll.ScrollID}";
                        cmbScrolls.Items.Add(thismapspace.ThisScroll);
                        scrollcount++;

                    }
                    else if ((resourcecount < 600) && (diceroll >= 28) && (diceroll < 69))
                    {
                        if (thismapspace.MapspaceResources.Count <= 2)
                        {
                            bool check = false;
                            while (!check)
                            {
                                thismapspace.ContainsResource = true;
                                Resource thisresource = new Resource();
                                int value = generator.Next(0, 4);
                                switch (value)
                                {
                                    case 0:
                                        thisresource.Resourcetype = "Wood";
                                        thismapspace.MapspaceResources.Add(thisresource);
                                        resourcecount++;
                                        break;
                                    case 1:
                                        thisresource.Resourcetype = "Metal";
                                        thismapspace.MapspaceResources.Add(thisresource);
                                        resourcecount++;
                                        break;
                                    case 2:
                                        thisresource.Resourcetype = "Jade";
                                        thismapspace.MapspaceResources.Add(thisresource);
                                        resourcecount++;
                                        break;
                                    case 3:
                                        thisresource.Resourcetype = "Tooth";
                                        thismapspace.MapspaceResources.Add(thisresource);
                                        resourcecount++;
                                        break;
                                    default:
                                        break;
                                }

                                if (thismapspace.MapspaceResources.Count > 2)
                                {
                                    check = true;
                                }
                            }
                        }
                    }

                }

            }
            foreach (Mapspace thismapspot in thismap)
            {
                cmbResources.Items.Add(thismapspot.MapspaceResources);
            }
            UpdatePlayerPosition();
            cmbMonkField.Enabled = false;
        }
        private void btnGather_Click(object sender, EventArgs e)
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            thismapspace = thismap[i, j];

            if ((user.Backpack.Count < 11) && (thismapspace.MapspaceResources.Count < (11 - user.Backpack.Count)))
            {
                if (thismapspace.ContainsMonk)
                {
                    if (cmbMonkField.SelectedIndex != -1)
                    {
                        switch (cmbMonkField.SelectedItem)
                        {
                            case "Backpack Monk":
                                picBackpackMonk.Image = Image.FromFile("Monk.jpg");
                                picBackpackMonk.SizeMode = PictureBoxSizeMode.StretchImage;
                                user.Backpackmonk = true;
                                break;
                            case "Sacred Monk":
                                picSacredMonk.Image = Image.FromFile("Monk.jpg");
                                picSacredMonk.SizeMode = PictureBoxSizeMode.StretchImage;
                                user.Sacredmonk = true;
                                break;
                            case "Weapon Monk":
                                picWeaponMonk.Image = Image.FromFile("Monk.jpg");
                                picWeaponMonk.SizeMode = PictureBoxSizeMode.StretchImage;
                                user.Weaponmonk = true;
                                break;
                            case "Action Monk":
                                picActionMonk.Image = Image.FromFile("Monk.jpg");
                                picActionMonk.SizeMode = PictureBoxSizeMode.StretchImage;
                                user.Actionmonk = true;
                                break;
                            default:
                                break;
                        }
                        user.CurrentActiondiscs--;
                        user = user.GatherPlayer(user, ref thismap);
                        UpdatePlayerPosition();
                    }
                    else
                    {
                        MessageBox.Show("Selecteer Veld voor Monk in ComboBox", "Ela Maat", MessageBoxButtons.OK);
                    }

                }
                else
                {
                    user.CurrentActiondiscs--;
                    user = user.GatherPlayer(user, ref thismap);
                    UpdatePlayerPosition();
                }
            }
            else
            {
                MessageBox.Show("Backpack VOL! of Resources op deze square", "Hola Vriend", MessageBoxButtons.OK);
            }

        }
        private void btnRest_Click(object sender, EventArgs e)
        {
            user.CurrentActiondiscs--;
            Frm_WeaponDesigner thisweapondesign = new Frm_WeaponDesigner(user);
            this.AddOwnedForm(thisweapondesign);
            if (thisweapondesign.ShowDialog() == DialogResult.OK)
            {
                UpdatePlayerPosition();
            }
            else
            {
                thisweapondesign.Close();
                UpdatePlayerPosition();
            }
        }
        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            thismap = user.MoveUp(user, thismap);
            user.CurrentMovepoints--;
            user = UpdatePlayerPosition();
        }
        private void btnMoveRight_Click(object sender, EventArgs e)
        {

            thismap = user.MoveRight(user, thismap);
            user.CurrentMovepoints--;
            user = UpdatePlayerPosition();
        }
        private void btnMoveLeft_Click(object sender, EventArgs e)
        {

            thismap = user.MoveLeft(user, thismap);
            user.CurrentMovepoints--;
            user = UpdatePlayerPosition();
        }
        private void btnMoveDown_Click(object sender, EventArgs e)
        {

            thismap = user.MoveDown(user, thismap);
            user.CurrentMovepoints--;
            user = UpdatePlayerPosition();
        }
        public Player UpdatePlayerPosition()
        {
            string output = "";
            ResetButtons();
            if (user.PlayerpositionXaxis == 0)
            {
                btnMoveUp.Enabled = false;
            }
            else if (user.PlayerpositionXaxis == 14)
            {
                btnMoveDown.Enabled = false;
            }
            if (user.PlayerpositionYaxis == 0)
            {
                btnMoveLeft.Enabled = false;
            }
            else if (user.PlayerpositionYaxis == 14)
            {
                btnMoveRight.Enabled = false;
            }
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    if ((((i == ((user.PlayerpositionXaxis) + 1)) || (i == ((user.PlayerpositionXaxis) - 1))) && (j == ((user.PlayerpositionYaxis)))) || ((((j == ((user.PlayerpositionYaxis) + 1)) || (j == ((user.PlayerpositionYaxis) - 1)))) && (i == (user.PlayerpositionXaxis))))
                    {
                        Mapspace thismapspace = thismap[i, j];
                        if (thismapspace.ContainsDragon)
                        {
                            MessageBox.Show("Draak binnen 1 vak, combat w getriggered", "I R GOD", MessageBoxButtons.OK);
                            Frm_Combat thiscombat = new Frm_Combat(user, thismapspace.MapspaceDragon);
                            this.AddOwnedForm(thiscombat);
                            if (thiscombat.ShowDialog() == DialogResult.OK)
                            {
                                thismapspace.ContainsDragon = false;
                                thismapspace.MapspaceDragon = null;
                                thiscombat.Close();
                                Dragonegg thisegg = new Dragonegg();
                                thismapspace.ContainsEgg = true;
                                thismapspace.ThisEgg = thisegg;
                            }
                        }
                        thismap[i, j] = thismapspace;
                    }
                }
            }
            output = thismapspace.DisplayMap(thismap, user);
            txtMap.Text = output;
            txtStatusPlayer.Text = user.UpdateStatusWindow(user);
            Mapspace mapspacestatus = new Mapspace();
            mapspacestatus = thismap[user.PlayerpositionXaxis, user.PlayerpositionYaxis];
            output = mapspacestatus.MapspaceStatus(mapspacestatus);
            txtSpaceInfo.Text = output;
            output = "";
            lbBackpack.DataSource = null;
            lbBackpack.DataSource = user.Backpack;
            lblRoundCounter.Text = roundcounter.ToString();
            UpdateMonksSelection();
            DisplayWeapon(user.Weapon);
            return user;
        }
        public void ResetButtons()
        {
            if ((user.CurrentMovepoints > 0) && (user.CurrentActiondiscs == 0))
            {
                btnMoveLeft.Enabled = true;
                btnMoveRight.Enabled = true;
                btnMoveUp.Enabled = true;
                btnMoveDown.Enabled = true;
                btnGather.Enabled = false;
                btnMove.Enabled = false;
                btnRest.Enabled = false;
            }
            else if (user.CurrentActiondiscs == 0)
            {
                btnMoveLeft.Enabled = false;
                btnMoveRight.Enabled = false;
                btnMoveUp.Enabled = false;
                btnMoveDown.Enabled = false;
                btnGather.Enabled = false;
                btnMove.Enabled = false;
                btnRest.Enabled = false;
            }
            else if (user.CurrentMovepoints > 0)
            {
                btnMoveLeft.Enabled = true;
                btnMoveRight.Enabled = true;
                btnMoveUp.Enabled = true;
                btnMoveDown.Enabled = true;
                btnGather.Enabled = true;
                btnMove.Enabled = true;
                btnRest.Enabled = true;
            }
            else
            {
                btnMoveLeft.Enabled = false;
                btnMoveRight.Enabled = false;
                btnMoveUp.Enabled = false;
                btnMoveDown.Enabled = false;
                btnGather.Enabled = true;
                btnMove.Enabled = true;
                btnRest.Enabled = true;
            }
        }
        private void btnMove_Click(object sender, EventArgs e)
        {
            user.CurrentActiondiscs--;
            user.CurrentMovepoints = (user.CurrentMovepoints + 2);
            UpdatePlayerPosition();
            ResetButtons();
        }
        private void btnEndRound_Click(object sender, EventArgs e)
        {
            roundcounter++;
            lblRoundCounter.Text = roundcounter.ToString(); ;
            user.CurrentActiondiscs = user.MaxActiondiscs;
            UpdatePlayerPosition();
        }
        private void btnEmptySelectionBackpack_Click(object sender, EventArgs e)
        {
            var itemtodelete = lbBackpack.SelectedItem;
            lbBackpack.DataSource = null;
            user.Backpack.Remove(itemtodelete);
            lbBackpack.DataSource = user.Backpack;
            lbBackpack.SelectedIndex = -1;
        }
        private void btnEndSession_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
        private void UpdateMonksSelection()
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thismapspace = thismap[i, j];
            if (thismapspace.ContainsMonk)
            {
                cmbMonkField.Items.Clear();
                cmbMonkField.Enabled = true;
                if (!user.Actionmonk)
                {
                    cmbMonkField.Items.Add("Action Monk");
                }
                if (!user.Backpackmonk)
                {
                    cmbMonkField.Items.Add("Backpack Monk");
                }
                if (!user.Sacredmonk)
                {
                    cmbMonkField.Items.Add("Sacred Monk");
                }
                if (!user.Weaponmonk)
                {
                    cmbMonkField.Items.Add("WeaponMonk");
                }
            }
            else
            {
                cmbMonkField.Enabled = false;
            }
        }
        public void DisplayWeapon(Resource[,] thisweapon)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Resource myweaponpart = thisweapon[i, j];
                    switch (j)
                    {
                        case 0:

                            UpdatepictureBoxesList1(i);
                            break;
                        case 1:

                            UpdatepictureBoxesList2(i);
                            break;
                        case 2:

                            UpdatepictureBoxesList3(i);
                            break;
                        case 3:

                            UpdatepictureBoxesList4(i);
                            break;
                        case 4:

                            UpdatepictureBoxesList5(i);
                            break;
                        case 5:

                            UpdatepictureBoxesList6(i);
                            break;
                        case 6:

                            UpdatepictureBoxesList7(i);
                            break;
                        case 7:

                            UpdatepictureBoxesList8(i);
                            break;
                        case 8:

                            UpdatepictureBoxesList9(i);
                            break;
                        default:
                            break;
                    }
                }
            }

        }
        private void UpdatepictureBoxesList1(int i)
        {
            string answer;
            switch (user.Weapon[i, 0].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList1("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList1("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList1("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList1("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList2(int i)
        {
            string answer;
            switch (user.Weapon[i, 1].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList2("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList2("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList2("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList2("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList3(int i)
        {
            string answer;
            switch (user.Weapon[i, 2].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList3("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList3("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList3("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList3("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList4(int i)
        {
            string answer;
            switch (user.Weapon[i, 3].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList4("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList4("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList4("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList4("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList5(int i)
        {
            string answer;
            switch (user.Weapon[i, 4].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList5("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList5("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList5("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList5("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList6(int i)
        {
            string answer;
            switch (user.Weapon[i, 5].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList6("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList6("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList6("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList6("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList7(int i)
        {
            string answer;
            switch (user.Weapon[i, 6].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList7("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList7("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList7("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList7("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList8(int i)
        {
            string answer;
            switch (user.Weapon[i, 7].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList8("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList8("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList8("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList8("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList9(int i)
        {
            string answer;
            switch (user.Weapon[i, 8].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList9("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList9("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList9("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList9("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList1(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic00.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic10.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic20.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic30.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic40.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList2(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic01.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic11.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic21.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic31.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic41.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList3(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic02.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic12.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic22.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic32.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic42.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList4(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic03.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic13.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic23.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic33.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic43.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList5(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic04.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic14.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic24.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic34.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic44.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList6(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic05.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic15.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic25.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic35.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic45.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList7(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic06.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic16.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic26.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic36.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic46.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList8(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic07.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic17.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic27.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic37.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic47.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList9(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic08.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic18.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic28.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic38.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic48.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
    }
}
