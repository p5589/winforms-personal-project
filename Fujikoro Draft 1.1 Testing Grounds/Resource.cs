﻿using System.Collections.Generic;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public class Resource
    {

        private string _resourcetype;

        public Resource()
        {

        }


        public string Resourcetype
        {
            get { return _resourcetype; }
            set { _resourcetype = value; }
        }
        public string StatusWindow(Mapspace thismapspace)
        {
            string output = "";
            foreach (Resource thisresource in thismapspace.MapspaceResources)
            {
                output += $"{thisresource.Resourcetype}";
            }
            return output;
        }

        public string ResourcetoMap(List<Resource> mapspotresources)
        {
            string output = "";
            foreach (Resource thisresource in mapspotresources)
            {
                switch (thisresource.Resourcetype)
                {
                    case "Wood":
                        output += "w";
                        break;
                    case "Metal":
                        output += "m";
                        break;
                    case "Jade":
                        output += "j";
                        break;
                    case "Tooth":
                        output += "t";
                        break;
                    default:
                        break;
                }
            }
            return output;
        }
        public override string ToString()
        {
            return $"{Resourcetype}";
        }
    }
}
