﻿
namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    partial class Frm_WeaponDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbWeaponRow1 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow2 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow3 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow4 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow5 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow6 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow7 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow8 = new System.Windows.Forms.ListBox();
            this.lbWeaponRow9 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbBackpack = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBackpackPullHandle = new System.Windows.Forms.Button();
            this.btnCraft = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnAbort = new System.Windows.Forms.Button();
            this.btnConfirmTotal = new System.Windows.Forms.Button();
            this.btnBackpackPullBlade = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnConfirmFuse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbDualwielding = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pic48 = new System.Windows.Forms.PictureBox();
            this.pic47 = new System.Windows.Forms.PictureBox();
            this.pic46 = new System.Windows.Forms.PictureBox();
            this.pic45 = new System.Windows.Forms.PictureBox();
            this.pic44 = new System.Windows.Forms.PictureBox();
            this.pic43 = new System.Windows.Forms.PictureBox();
            this.pic42 = new System.Windows.Forms.PictureBox();
            this.pic41 = new System.Windows.Forms.PictureBox();
            this.pic40 = new System.Windows.Forms.PictureBox();
            this.pic38 = new System.Windows.Forms.PictureBox();
            this.pic37 = new System.Windows.Forms.PictureBox();
            this.pic36 = new System.Windows.Forms.PictureBox();
            this.pic35 = new System.Windows.Forms.PictureBox();
            this.pic34 = new System.Windows.Forms.PictureBox();
            this.pic33 = new System.Windows.Forms.PictureBox();
            this.pic32 = new System.Windows.Forms.PictureBox();
            this.pic31 = new System.Windows.Forms.PictureBox();
            this.pic30 = new System.Windows.Forms.PictureBox();
            this.pic28 = new System.Windows.Forms.PictureBox();
            this.pic27 = new System.Windows.Forms.PictureBox();
            this.pic26 = new System.Windows.Forms.PictureBox();
            this.pic25 = new System.Windows.Forms.PictureBox();
            this.pic24 = new System.Windows.Forms.PictureBox();
            this.pic23 = new System.Windows.Forms.PictureBox();
            this.pic22 = new System.Windows.Forms.PictureBox();
            this.pic21 = new System.Windows.Forms.PictureBox();
            this.pic20 = new System.Windows.Forms.PictureBox();
            this.pic18 = new System.Windows.Forms.PictureBox();
            this.pic17 = new System.Windows.Forms.PictureBox();
            this.pic16 = new System.Windows.Forms.PictureBox();
            this.pic15 = new System.Windows.Forms.PictureBox();
            this.pic14 = new System.Windows.Forms.PictureBox();
            this.pic13 = new System.Windows.Forms.PictureBox();
            this.pic12 = new System.Windows.Forms.PictureBox();
            this.pic11 = new System.Windows.Forms.PictureBox();
            this.pic10 = new System.Windows.Forms.PictureBox();
            this.pic08 = new System.Windows.Forms.PictureBox();
            this.pic07 = new System.Windows.Forms.PictureBox();
            this.pic06 = new System.Windows.Forms.PictureBox();
            this.pic05 = new System.Windows.Forms.PictureBox();
            this.pic04 = new System.Windows.Forms.PictureBox();
            this.pic03 = new System.Windows.Forms.PictureBox();
            this.pic02 = new System.Windows.Forms.PictureBox();
            this.pic01 = new System.Windows.Forms.PictureBox();
            this.pic00 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic00)).BeginInit();
            this.SuspendLayout();
            // 
            // lbWeaponRow1
            // 
            this.lbWeaponRow1.FormattingEnabled = true;
            this.lbWeaponRow1.ItemHeight = 20;
            this.lbWeaponRow1.Location = new System.Drawing.Point(35, 25);
            this.lbWeaponRow1.Name = "lbWeaponRow1";
            this.lbWeaponRow1.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow1.TabIndex = 0;
            this.lbWeaponRow1.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow1_SelectedIndexChanged);
            // 
            // lbWeaponRow2
            // 
            this.lbWeaponRow2.FormattingEnabled = true;
            this.lbWeaponRow2.ItemHeight = 20;
            this.lbWeaponRow2.Location = new System.Drawing.Point(97, 25);
            this.lbWeaponRow2.Name = "lbWeaponRow2";
            this.lbWeaponRow2.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow2.TabIndex = 1;
            this.lbWeaponRow2.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow2_SelectedIndexChanged);
            // 
            // lbWeaponRow3
            // 
            this.lbWeaponRow3.FormattingEnabled = true;
            this.lbWeaponRow3.ItemHeight = 20;
            this.lbWeaponRow3.Location = new System.Drawing.Point(159, 25);
            this.lbWeaponRow3.Name = "lbWeaponRow3";
            this.lbWeaponRow3.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow3.TabIndex = 2;
            this.lbWeaponRow3.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow3_SelectedIndexChanged);
            // 
            // lbWeaponRow4
            // 
            this.lbWeaponRow4.FormattingEnabled = true;
            this.lbWeaponRow4.ItemHeight = 20;
            this.lbWeaponRow4.Location = new System.Drawing.Point(221, 25);
            this.lbWeaponRow4.Name = "lbWeaponRow4";
            this.lbWeaponRow4.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow4.TabIndex = 3;
            this.lbWeaponRow4.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow4_SelectedIndexChanged);
            // 
            // lbWeaponRow5
            // 
            this.lbWeaponRow5.FormattingEnabled = true;
            this.lbWeaponRow5.ItemHeight = 20;
            this.lbWeaponRow5.Location = new System.Drawing.Point(283, 25);
            this.lbWeaponRow5.Name = "lbWeaponRow5";
            this.lbWeaponRow5.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow5.TabIndex = 4;
            this.lbWeaponRow5.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow5_SelectedIndexChanged);
            // 
            // lbWeaponRow6
            // 
            this.lbWeaponRow6.FormattingEnabled = true;
            this.lbWeaponRow6.ItemHeight = 20;
            this.lbWeaponRow6.Location = new System.Drawing.Point(345, 25);
            this.lbWeaponRow6.Name = "lbWeaponRow6";
            this.lbWeaponRow6.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow6.TabIndex = 5;
            this.lbWeaponRow6.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow6_SelectedIndexChanged);
            // 
            // lbWeaponRow7
            // 
            this.lbWeaponRow7.FormattingEnabled = true;
            this.lbWeaponRow7.ItemHeight = 20;
            this.lbWeaponRow7.Location = new System.Drawing.Point(407, 25);
            this.lbWeaponRow7.Name = "lbWeaponRow7";
            this.lbWeaponRow7.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow7.TabIndex = 6;
            this.lbWeaponRow7.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow7_SelectedIndexChanged);
            // 
            // lbWeaponRow8
            // 
            this.lbWeaponRow8.FormattingEnabled = true;
            this.lbWeaponRow8.ItemHeight = 20;
            this.lbWeaponRow8.Location = new System.Drawing.Point(469, 25);
            this.lbWeaponRow8.Name = "lbWeaponRow8";
            this.lbWeaponRow8.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow8.TabIndex = 7;
            this.lbWeaponRow8.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow8_SelectedIndexChanged);
            // 
            // lbWeaponRow9
            // 
            this.lbWeaponRow9.FormattingEnabled = true;
            this.lbWeaponRow9.ItemHeight = 20;
            this.lbWeaponRow9.Location = new System.Drawing.Point(531, 25);
            this.lbWeaponRow9.Name = "lbWeaponRow9";
            this.lbWeaponRow9.Size = new System.Drawing.Size(56, 164);
            this.lbWeaponRow9.TabIndex = 8;
            this.lbWeaponRow9.SelectedIndexChanged += new System.EventHandler(this.lbWeaponRow9_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbWeaponRow9);
            this.groupBox1.Controls.Add(this.lbWeaponRow8);
            this.groupBox1.Controls.Add(this.lbWeaponRow7);
            this.groupBox1.Controls.Add(this.lbWeaponRow6);
            this.groupBox1.Controls.Add(this.lbWeaponRow5);
            this.groupBox1.Controls.Add(this.lbWeaponRow4);
            this.groupBox1.Controls.Add(this.lbWeaponRow3);
            this.groupBox1.Controls.Add(this.lbWeaponRow2);
            this.groupBox1.Controls.Add(this.lbWeaponRow1);
            this.groupBox1.Location = new System.Drawing.Point(87, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(643, 221);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weapon:";
            // 
            // lbBackpack
            // 
            this.lbBackpack.FormattingEnabled = true;
            this.lbBackpack.ItemHeight = 20;
            this.lbBackpack.Location = new System.Drawing.Point(807, 74);
            this.lbBackpack.Name = "lbBackpack";
            this.lbBackpack.Size = new System.Drawing.Size(195, 324);
            this.lbBackpack.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(807, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Backpack:";
            // 
            // btnBackpackPullHandle
            // 
            this.btnBackpackPullHandle.Location = new System.Drawing.Point(606, 303);
            this.btnBackpackPullHandle.Name = "btnBackpackPullHandle";
            this.btnBackpackPullHandle.Size = new System.Drawing.Size(195, 52);
            this.btnBackpackPullHandle.TabIndex = 12;
            this.btnBackpackPullHandle.Text = "Add Selected Item to Handle";
            this.btnBackpackPullHandle.UseVisualStyleBackColor = true;
            this.btnBackpackPullHandle.Click += new System.EventHandler(this.btnBackpackPull_Click);
            // 
            // btnCraft
            // 
            this.btnCraft.Location = new System.Drawing.Point(427, 303);
            this.btnCraft.Name = "btnCraft";
            this.btnCraft.Size = new System.Drawing.Size(173, 52);
            this.btnCraft.TabIndex = 13;
            this.btnCraft.Text = "Confirm Craft Handle";
            this.btnCraft.UseVisualStyleBackColor = true;
            this.btnCraft.Click += new System.EventHandler(this.btnCraft_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(835, 421);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(195, 52);
            this.button3.TabIndex = 14;
            this.button3.Text = "Remove Selected Item";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnAbort
            // 
            this.btnAbort.Location = new System.Drawing.Point(402, 12);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(179, 52);
            this.btnAbort.TabIndex = 15;
            this.btnAbort.Text = "Back to Map";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // btnConfirmTotal
            // 
            this.btnConfirmTotal.Location = new System.Drawing.Point(427, 365);
            this.btnConfirmTotal.Name = "btnConfirmTotal";
            this.btnConfirmTotal.Size = new System.Drawing.Size(173, 51);
            this.btnConfirmTotal.TabIndex = 16;
            this.btnConfirmTotal.Text = "ConfirmTotalWeapon";
            this.btnConfirmTotal.UseVisualStyleBackColor = true;
            this.btnConfirmTotal.Click += new System.EventHandler(this.btnConfirmTotal_Click);
            // 
            // btnBackpackPullBlade
            // 
            this.btnBackpackPullBlade.Location = new System.Drawing.Point(606, 362);
            this.btnBackpackPullBlade.Name = "btnBackpackPullBlade";
            this.btnBackpackPullBlade.Size = new System.Drawing.Size(195, 54);
            this.btnBackpackPullBlade.TabIndex = 17;
            this.btnBackpackPullBlade.Text = "Add Selected item to Blade";
            this.btnBackpackPullBlade.UseVisualStyleBackColor = true;
            this.btnBackpackPullBlade.Click += new System.EventHandler(this.btnBackpackPullBlade_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(606, 423);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(195, 54);
            this.button1.TabIndex = 18;
            this.button1.Text = "Add to Fuse 2 to 1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnConfirmFuse
            // 
            this.btnConfirmFuse.Location = new System.Drawing.Point(427, 422);
            this.btnConfirmFuse.Name = "btnConfirmFuse";
            this.btnConfirmFuse.Size = new System.Drawing.Size(173, 51);
            this.btnConfirmFuse.TabIndex = 19;
            this.btnConfirmFuse.Text = "Confirm Fuse\r\n";
            this.btnConfirmFuse.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 20);
            this.label2.TabIndex = 20;
            this.label2.Text = "Dual wielding :";
            // 
            // lbDualwielding
            // 
            this.lbDualwielding.AutoSize = true;
            this.lbDualwielding.Location = new System.Drawing.Point(203, 9);
            this.lbDualwielding.Name = "lbDualwielding";
            this.lbDualwielding.Size = new System.Drawing.Size(44, 20);
            this.lbDualwielding.TabIndex = 21;
            this.lbDualwielding.Text = "False;";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pic48);
            this.groupBox2.Controls.Add(this.pic47);
            this.groupBox2.Controls.Add(this.pic46);
            this.groupBox2.Controls.Add(this.pic45);
            this.groupBox2.Controls.Add(this.pic44);
            this.groupBox2.Controls.Add(this.pic43);
            this.groupBox2.Controls.Add(this.pic42);
            this.groupBox2.Controls.Add(this.pic41);
            this.groupBox2.Controls.Add(this.pic40);
            this.groupBox2.Controls.Add(this.pic38);
            this.groupBox2.Controls.Add(this.pic37);
            this.groupBox2.Controls.Add(this.pic36);
            this.groupBox2.Controls.Add(this.pic35);
            this.groupBox2.Controls.Add(this.pic34);
            this.groupBox2.Controls.Add(this.pic33);
            this.groupBox2.Controls.Add(this.pic32);
            this.groupBox2.Controls.Add(this.pic31);
            this.groupBox2.Controls.Add(this.pic30);
            this.groupBox2.Controls.Add(this.pic28);
            this.groupBox2.Controls.Add(this.pic27);
            this.groupBox2.Controls.Add(this.pic26);
            this.groupBox2.Controls.Add(this.pic25);
            this.groupBox2.Controls.Add(this.pic24);
            this.groupBox2.Controls.Add(this.pic23);
            this.groupBox2.Controls.Add(this.pic22);
            this.groupBox2.Controls.Add(this.pic21);
            this.groupBox2.Controls.Add(this.pic20);
            this.groupBox2.Controls.Add(this.pic18);
            this.groupBox2.Controls.Add(this.pic17);
            this.groupBox2.Controls.Add(this.pic16);
            this.groupBox2.Controls.Add(this.pic15);
            this.groupBox2.Controls.Add(this.pic14);
            this.groupBox2.Controls.Add(this.pic13);
            this.groupBox2.Controls.Add(this.pic12);
            this.groupBox2.Controls.Add(this.pic11);
            this.groupBox2.Controls.Add(this.pic10);
            this.groupBox2.Controls.Add(this.pic08);
            this.groupBox2.Controls.Add(this.pic07);
            this.groupBox2.Controls.Add(this.pic06);
            this.groupBox2.Controls.Add(this.pic05);
            this.groupBox2.Controls.Add(this.pic04);
            this.groupBox2.Controls.Add(this.pic03);
            this.groupBox2.Controls.Add(this.pic02);
            this.groupBox2.Controls.Add(this.pic01);
            this.groupBox2.Controls.Add(this.pic00);
            this.groupBox2.Location = new System.Drawing.Point(12, 303);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(414, 279);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // pic48
            // 
            this.pic48.Location = new System.Drawing.Point(354, 212);
            this.pic48.Name = "pic48";
            this.pic48.Size = new System.Drawing.Size(37, 40);
            this.pic48.TabIndex = 44;
            this.pic48.TabStop = false;
            // 
            // pic47
            // 
            this.pic47.Location = new System.Drawing.Point(307, 212);
            this.pic47.Name = "pic47";
            this.pic47.Size = new System.Drawing.Size(37, 40);
            this.pic47.TabIndex = 43;
            this.pic47.TabStop = false;
            // 
            // pic46
            // 
            this.pic46.Location = new System.Drawing.Point(264, 212);
            this.pic46.Name = "pic46";
            this.pic46.Size = new System.Drawing.Size(37, 40);
            this.pic46.TabIndex = 42;
            this.pic46.TabStop = false;
            // 
            // pic45
            // 
            this.pic45.Location = new System.Drawing.Point(221, 212);
            this.pic45.Name = "pic45";
            this.pic45.Size = new System.Drawing.Size(37, 40);
            this.pic45.TabIndex = 41;
            this.pic45.TabStop = false;
            // 
            // pic44
            // 
            this.pic44.Location = new System.Drawing.Point(178, 212);
            this.pic44.Name = "pic44";
            this.pic44.Size = new System.Drawing.Size(37, 40);
            this.pic44.TabIndex = 40;
            this.pic44.TabStop = false;
            // 
            // pic43
            // 
            this.pic43.Location = new System.Drawing.Point(135, 212);
            this.pic43.Name = "pic43";
            this.pic43.Size = new System.Drawing.Size(37, 40);
            this.pic43.TabIndex = 39;
            this.pic43.TabStop = false;
            // 
            // pic42
            // 
            this.pic42.Location = new System.Drawing.Point(92, 212);
            this.pic42.Name = "pic42";
            this.pic42.Size = new System.Drawing.Size(37, 40);
            this.pic42.TabIndex = 38;
            this.pic42.TabStop = false;
            // 
            // pic41
            // 
            this.pic41.Location = new System.Drawing.Point(49, 212);
            this.pic41.Name = "pic41";
            this.pic41.Size = new System.Drawing.Size(37, 40);
            this.pic41.TabIndex = 37;
            this.pic41.TabStop = false;
            // 
            // pic40
            // 
            this.pic40.Location = new System.Drawing.Point(6, 212);
            this.pic40.Name = "pic40";
            this.pic40.Size = new System.Drawing.Size(37, 40);
            this.pic40.TabIndex = 36;
            this.pic40.TabStop = false;
            // 
            // pic38
            // 
            this.pic38.Location = new System.Drawing.Point(354, 166);
            this.pic38.Name = "pic38";
            this.pic38.Size = new System.Drawing.Size(37, 40);
            this.pic38.TabIndex = 35;
            this.pic38.TabStop = false;
            // 
            // pic37
            // 
            this.pic37.Location = new System.Drawing.Point(311, 166);
            this.pic37.Name = "pic37";
            this.pic37.Size = new System.Drawing.Size(37, 40);
            this.pic37.TabIndex = 34;
            this.pic37.TabStop = false;
            // 
            // pic36
            // 
            this.pic36.Location = new System.Drawing.Point(264, 166);
            this.pic36.Name = "pic36";
            this.pic36.Size = new System.Drawing.Size(37, 40);
            this.pic36.TabIndex = 33;
            this.pic36.TabStop = false;
            // 
            // pic35
            // 
            this.pic35.Location = new System.Drawing.Point(221, 166);
            this.pic35.Name = "pic35";
            this.pic35.Size = new System.Drawing.Size(37, 40);
            this.pic35.TabIndex = 32;
            this.pic35.TabStop = false;
            // 
            // pic34
            // 
            this.pic34.Location = new System.Drawing.Point(178, 166);
            this.pic34.Name = "pic34";
            this.pic34.Size = new System.Drawing.Size(37, 40);
            this.pic34.TabIndex = 31;
            this.pic34.TabStop = false;
            // 
            // pic33
            // 
            this.pic33.Location = new System.Drawing.Point(135, 166);
            this.pic33.Name = "pic33";
            this.pic33.Size = new System.Drawing.Size(37, 40);
            this.pic33.TabIndex = 30;
            this.pic33.TabStop = false;
            // 
            // pic32
            // 
            this.pic32.Location = new System.Drawing.Point(92, 166);
            this.pic32.Name = "pic32";
            this.pic32.Size = new System.Drawing.Size(37, 40);
            this.pic32.TabIndex = 29;
            this.pic32.TabStop = false;
            // 
            // pic31
            // 
            this.pic31.Location = new System.Drawing.Point(49, 166);
            this.pic31.Name = "pic31";
            this.pic31.Size = new System.Drawing.Size(37, 40);
            this.pic31.TabIndex = 28;
            this.pic31.TabStop = false;
            // 
            // pic30
            // 
            this.pic30.Location = new System.Drawing.Point(6, 166);
            this.pic30.Name = "pic30";
            this.pic30.Size = new System.Drawing.Size(37, 40);
            this.pic30.TabIndex = 27;
            this.pic30.TabStop = false;
            // 
            // pic28
            // 
            this.pic28.Location = new System.Drawing.Point(354, 120);
            this.pic28.Name = "pic28";
            this.pic28.Size = new System.Drawing.Size(37, 40);
            this.pic28.TabIndex = 26;
            this.pic28.TabStop = false;
            // 
            // pic27
            // 
            this.pic27.Location = new System.Drawing.Point(311, 120);
            this.pic27.Name = "pic27";
            this.pic27.Size = new System.Drawing.Size(37, 40);
            this.pic27.TabIndex = 25;
            this.pic27.TabStop = false;
            // 
            // pic26
            // 
            this.pic26.Location = new System.Drawing.Point(268, 120);
            this.pic26.Name = "pic26";
            this.pic26.Size = new System.Drawing.Size(37, 40);
            this.pic26.TabIndex = 24;
            this.pic26.TabStop = false;
            // 
            // pic25
            // 
            this.pic25.Location = new System.Drawing.Point(221, 120);
            this.pic25.Name = "pic25";
            this.pic25.Size = new System.Drawing.Size(37, 40);
            this.pic25.TabIndex = 23;
            this.pic25.TabStop = false;
            // 
            // pic24
            // 
            this.pic24.Location = new System.Drawing.Point(178, 120);
            this.pic24.Name = "pic24";
            this.pic24.Size = new System.Drawing.Size(37, 40);
            this.pic24.TabIndex = 22;
            this.pic24.TabStop = false;
            // 
            // pic23
            // 
            this.pic23.Location = new System.Drawing.Point(135, 120);
            this.pic23.Name = "pic23";
            this.pic23.Size = new System.Drawing.Size(37, 40);
            this.pic23.TabIndex = 21;
            this.pic23.TabStop = false;
            // 
            // pic22
            // 
            this.pic22.Location = new System.Drawing.Point(92, 120);
            this.pic22.Name = "pic22";
            this.pic22.Size = new System.Drawing.Size(37, 40);
            this.pic22.TabIndex = 20;
            this.pic22.TabStop = false;
            // 
            // pic21
            // 
            this.pic21.Location = new System.Drawing.Point(49, 120);
            this.pic21.Name = "pic21";
            this.pic21.Size = new System.Drawing.Size(37, 40);
            this.pic21.TabIndex = 19;
            this.pic21.TabStop = false;
            // 
            // pic20
            // 
            this.pic20.Location = new System.Drawing.Point(6, 120);
            this.pic20.Name = "pic20";
            this.pic20.Size = new System.Drawing.Size(37, 40);
            this.pic20.TabIndex = 18;
            this.pic20.TabStop = false;
            // 
            // pic18
            // 
            this.pic18.Location = new System.Drawing.Point(354, 72);
            this.pic18.Name = "pic18";
            this.pic18.Size = new System.Drawing.Size(37, 40);
            this.pic18.TabIndex = 17;
            this.pic18.TabStop = false;
            // 
            // pic17
            // 
            this.pic17.Location = new System.Drawing.Point(311, 71);
            this.pic17.Name = "pic17";
            this.pic17.Size = new System.Drawing.Size(37, 40);
            this.pic17.TabIndex = 16;
            this.pic17.TabStop = false;
            // 
            // pic16
            // 
            this.pic16.Location = new System.Drawing.Point(268, 71);
            this.pic16.Name = "pic16";
            this.pic16.Size = new System.Drawing.Size(37, 40);
            this.pic16.TabIndex = 15;
            this.pic16.TabStop = false;
            // 
            // pic15
            // 
            this.pic15.Location = new System.Drawing.Point(221, 71);
            this.pic15.Name = "pic15";
            this.pic15.Size = new System.Drawing.Size(37, 40);
            this.pic15.TabIndex = 14;
            this.pic15.TabStop = false;
            // 
            // pic14
            // 
            this.pic14.Location = new System.Drawing.Point(178, 71);
            this.pic14.Name = "pic14";
            this.pic14.Size = new System.Drawing.Size(37, 40);
            this.pic14.TabIndex = 13;
            this.pic14.TabStop = false;
            // 
            // pic13
            // 
            this.pic13.Location = new System.Drawing.Point(135, 71);
            this.pic13.Name = "pic13";
            this.pic13.Size = new System.Drawing.Size(37, 40);
            this.pic13.TabIndex = 12;
            this.pic13.TabStop = false;
            // 
            // pic12
            // 
            this.pic12.Location = new System.Drawing.Point(92, 71);
            this.pic12.Name = "pic12";
            this.pic12.Size = new System.Drawing.Size(37, 40);
            this.pic12.TabIndex = 11;
            this.pic12.TabStop = false;
            // 
            // pic11
            // 
            this.pic11.Location = new System.Drawing.Point(49, 71);
            this.pic11.Name = "pic11";
            this.pic11.Size = new System.Drawing.Size(37, 40);
            this.pic11.TabIndex = 10;
            this.pic11.TabStop = false;
            // 
            // pic10
            // 
            this.pic10.Location = new System.Drawing.Point(6, 71);
            this.pic10.Name = "pic10";
            this.pic10.Size = new System.Drawing.Size(37, 40);
            this.pic10.TabIndex = 9;
            this.pic10.TabStop = false;
            // 
            // pic08
            // 
            this.pic08.Location = new System.Drawing.Point(354, 26);
            this.pic08.Name = "pic08";
            this.pic08.Size = new System.Drawing.Size(37, 40);
            this.pic08.TabIndex = 8;
            this.pic08.TabStop = false;
            // 
            // pic07
            // 
            this.pic07.Location = new System.Drawing.Point(311, 26);
            this.pic07.Name = "pic07";
            this.pic07.Size = new System.Drawing.Size(37, 40);
            this.pic07.TabIndex = 7;
            this.pic07.TabStop = false;
            // 
            // pic06
            // 
            this.pic06.Location = new System.Drawing.Point(268, 26);
            this.pic06.Name = "pic06";
            this.pic06.Size = new System.Drawing.Size(37, 40);
            this.pic06.TabIndex = 6;
            this.pic06.TabStop = false;
            // 
            // pic05
            // 
            this.pic05.Location = new System.Drawing.Point(221, 26);
            this.pic05.Name = "pic05";
            this.pic05.Size = new System.Drawing.Size(37, 40);
            this.pic05.TabIndex = 5;
            this.pic05.TabStop = false;
            // 
            // pic04
            // 
            this.pic04.Location = new System.Drawing.Point(178, 26);
            this.pic04.Name = "pic04";
            this.pic04.Size = new System.Drawing.Size(37, 40);
            this.pic04.TabIndex = 4;
            this.pic04.TabStop = false;
            // 
            // pic03
            // 
            this.pic03.Location = new System.Drawing.Point(135, 26);
            this.pic03.Name = "pic03";
            this.pic03.Size = new System.Drawing.Size(37, 40);
            this.pic03.TabIndex = 3;
            this.pic03.TabStop = false;
            // 
            // pic02
            // 
            this.pic02.Location = new System.Drawing.Point(92, 26);
            this.pic02.Name = "pic02";
            this.pic02.Size = new System.Drawing.Size(37, 40);
            this.pic02.TabIndex = 2;
            this.pic02.TabStop = false;
            // 
            // pic01
            // 
            this.pic01.Location = new System.Drawing.Point(49, 26);
            this.pic01.Name = "pic01";
            this.pic01.Size = new System.Drawing.Size(37, 40);
            this.pic01.TabIndex = 1;
            this.pic01.TabStop = false;
            // 
            // pic00
            // 
            this.pic00.Location = new System.Drawing.Point(6, 26);
            this.pic00.Name = "pic00";
            this.pic00.Size = new System.Drawing.Size(37, 40);
            this.pic00.TabIndex = 0;
            this.pic00.TabStop = false;
            // 
            // Frm_WeaponDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 630);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lbDualwielding);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnConfirmFuse);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBackpackPullBlade);
            this.Controls.Add(this.btnConfirmTotal);
            this.Controls.Add(this.btnAbort);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnCraft);
            this.Controls.Add(this.btnBackpackPullHandle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbBackpack);
            this.Controls.Add(this.groupBox1);
            this.Name = "Frm_WeaponDesigner";
            this.Text = "Frm_WeaponDesigner";
            this.Load += new System.EventHandler(this.Frm_WeaponDesigner_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic00)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbWeaponRow1;
        private System.Windows.Forms.ListBox lbWeaponRow2;
        private System.Windows.Forms.ListBox lbWeaponRow3;
        private System.Windows.Forms.ListBox lbWeaponRow4;
        private System.Windows.Forms.ListBox lbWeaponRow5;
        private System.Windows.Forms.ListBox lbWeaponRow6;
        private System.Windows.Forms.ListBox lbWeaponRow7;
        private System.Windows.Forms.ListBox lbWeaponRow8;
        private System.Windows.Forms.ListBox lbWeaponRow9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbBackpack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBackpackPullHandle;
        private System.Windows.Forms.Button btnCraft;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.Button btnConfirmTotal;
        private System.Windows.Forms.Button btnBackpackPullBlade;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnConfirmFuse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbDualwielding;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pic48;
        private System.Windows.Forms.PictureBox pic47;
        private System.Windows.Forms.PictureBox pic46;
        private System.Windows.Forms.PictureBox pic45;
        private System.Windows.Forms.PictureBox pic44;
        private System.Windows.Forms.PictureBox pic43;
        private System.Windows.Forms.PictureBox pic42;
        private System.Windows.Forms.PictureBox pic41;
        private System.Windows.Forms.PictureBox pic40;
        private System.Windows.Forms.PictureBox pic38;
        private System.Windows.Forms.PictureBox pic37;
        private System.Windows.Forms.PictureBox pic36;
        private System.Windows.Forms.PictureBox pic35;
        private System.Windows.Forms.PictureBox pic34;
        private System.Windows.Forms.PictureBox pic33;
        private System.Windows.Forms.PictureBox pic32;
        private System.Windows.Forms.PictureBox pic31;
        private System.Windows.Forms.PictureBox pic30;
        private System.Windows.Forms.PictureBox pic28;
        private System.Windows.Forms.PictureBox pic27;
        private System.Windows.Forms.PictureBox pic26;
        private System.Windows.Forms.PictureBox pic25;
        private System.Windows.Forms.PictureBox pic24;
        private System.Windows.Forms.PictureBox pic23;
        private System.Windows.Forms.PictureBox pic22;
        private System.Windows.Forms.PictureBox pic21;
        private System.Windows.Forms.PictureBox pic20;
        private System.Windows.Forms.PictureBox pic18;
        private System.Windows.Forms.PictureBox pic17;
        private System.Windows.Forms.PictureBox pic16;
        private System.Windows.Forms.PictureBox pic15;
        private System.Windows.Forms.PictureBox pic14;
        private System.Windows.Forms.PictureBox pic13;
        private System.Windows.Forms.PictureBox pic12;
        private System.Windows.Forms.PictureBox pic11;
        private System.Windows.Forms.PictureBox pic10;
        private System.Windows.Forms.PictureBox pic08;
        private System.Windows.Forms.PictureBox pic07;
        private System.Windows.Forms.PictureBox pic06;
        private System.Windows.Forms.PictureBox pic05;
        private System.Windows.Forms.PictureBox pic04;
        private System.Windows.Forms.PictureBox pic03;
        private System.Windows.Forms.PictureBox pic02;
        private System.Windows.Forms.PictureBox pic01;
        private System.Windows.Forms.PictureBox pic00;
    }
}