﻿using System.Collections;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public class Player
    {
        protected string _playermodel;
        private int _playerpositionXaxis = 0;
        private int _playerpositionYaxis = 0;
        private int _victorypoints = 0;
        private int _healthpoints = 2;
        private int _currentmovepoints = 0;
        private int _maxmovepoints = 2;
        private int _maxactiondiscs = 3;
        private int _currentactiondiscs = 3;
        private int _initiative = 0;
        private Resource[,] _weapon = new Resource[5, 9];
        private Resource[,] _helmet = new Resource[3, 3];
        private Resource[,] _sandals = new Resource[1, 3];
        private ArrayList _backpack = new ArrayList();
        private bool _dualwielding = false;
        private bool _actionmonk = false;
        private bool _sacredmonk = false;
        private bool _weaponmonk = false;
        private bool _backpackmonk = false;
        private Scroll _scrollmove = null;
        private Scroll _scrollgather = null;
        private Scroll _scrollexplore = null;
        private Scroll _scrollinventory1 = null;
        private Scroll _scrollinventory2 = null;

        public Player(string thiscolour)
        {
            _playermodel = thiscolour;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    Resource thisresource = new Resource();
                    thisresource.Resourcetype = "-";
                    _weapon[i, j] = thisresource;
                }
            }
            //basic weapon geven voor tests, lang genoeg etc om te breken, eventueel 2-hander rdy!
        }
        public string Playermodel
        {
            get { return _playermodel; }
        }
        public int PlayerpositionXaxis
        {
            get { return _playerpositionXaxis; }
            set { _playerpositionXaxis = value; }
        }
        public int PlayerpositionYaxis
        {
            get { return _playerpositionYaxis; }
            set { _playerpositionYaxis = value; }
        }
        public int Victorypoints
        {
            get { return _victorypoints; }
            set { _victorypoints = value; }
        }
        public int Healthpoints
        {
            get { return _healthpoints; }
            set { _healthpoints = value; }
        }
        public int MaxMovepoints
        {
            get { return _maxmovepoints; }
            set { _maxmovepoints = value; }
        }
        public int CurrentMovepoints
        {
            get { return _currentmovepoints; }
            set { _currentmovepoints = value; }
        }
        public int MaxActiondiscs
        {
            get { return _maxactiondiscs; }
            set { _maxactiondiscs = value; }
        }
        public int CurrentActiondiscs
        {
            get { return _currentactiondiscs; }
            set { _currentactiondiscs = value; }
        }

        public Resource[,] Weapon
        {
            get { return _weapon; }
            set { _weapon = value; }
        }
        public int Initiative
        {
            get { return _initiative; }
            set
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        if (_weapon[i, j] != null)
                        {
                            _initiative = j;
                        }
                    }
                }
                if ((Scroll1.ScrollID == 1) || (Scroll2.ScrollID == 1))
                {
                    _initiative++;
                }
            }
        }
        public Resource[,] Helmet
        {
            get { return _helmet; }
            set { _helmet = value; }
        }
        public Resource[,] Sandals
        {
            get { return _sandals; }
            set { _sandals = value; }
        }
        public ArrayList Backpack
        {
            get { return _backpack; }
            set { _backpack = value; }
        }
        public bool Actionmonk
        {
            get { return _actionmonk; }
            set { _actionmonk = value; }
        }
        public bool Sacredmonk
        {
            get { return _sacredmonk; }
            set { _sacredmonk = value; }
        }
        public bool Weaponmonk
        {
            get { return _weaponmonk; }
            set { _weaponmonk = value; }
        }
        public bool Backpackmonk
        {
            get { return _backpackmonk; }
            set { _backpackmonk = value; }
        }
        public bool Dualwielding
        {
            get { return _dualwielding; }
            set { _dualwielding = value; }
        }
        public Scroll Scroll1
        {
            get { return _scrollinventory1; }
            set { _scrollinventory1 = value; }
        }
        public Scroll Scroll2
        {
            get { return _scrollinventory2; }
            set { _scrollinventory2 = value; }
        }
        public override string ToString()
        {
            return $"{Playermodel} - leven : {Healthpoints} - VictoryP: {Victorypoints}";
        }
        public Mapspace[,] MoveUp(Player user, Mapspace[,] thismap)
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsPlayer = false;
            thisspace.MapspacePlayer = null;
            user.PlayerpositionXaxis--;
            i = user.PlayerpositionXaxis;
            thisspace = thismap[i, j];
            thisspace = user.UpdatePlayerPos(thisspace, user);
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Mapspace[,] MoveDown(Player user, Mapspace[,] thismap)
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsPlayer = false;
            thisspace.MapspacePlayer = null;
            user.PlayerpositionXaxis++;
            i = user.PlayerpositionXaxis;
            thisspace = thismap[i, j];
            thisspace = user.UpdatePlayerPos(thisspace, user);
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Mapspace[,] MoveLeft(Player user, Mapspace[,] thismap)
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsPlayer = false;
            thisspace.MapspacePlayer = null;
            user.PlayerpositionYaxis--;
            j = user.PlayerpositionYaxis;
            thisspace = thismap[i, j];
            thisspace = user.UpdatePlayerPos(thisspace, user);
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Mapspace[,] MoveRight(Player user, Mapspace[,] thismap)
        {

            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsPlayer = false;
            thisspace.MapspacePlayer = null;
            user.PlayerpositionYaxis++;
            j = user.PlayerpositionYaxis;
            thisspace = thismap[i, j];
            thisspace = user.UpdatePlayerPos(thisspace, user);
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Player GatherPlayer(Player user, ref Mapspace[,] thismap)
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            if (thisspace.ContainsMonk)
            {
                thismap = user.GatherMonksMap(user, thismap);
            }
            else if (thisspace.ContainsScroll)
            {
                if (user.Backpack.Count < 11)
                {
                    user.Backpack.Add(thisspace.ThisScroll);
                    thismap = user.GatherScrollsMap(user, thismap);
                }
            }
            else if (thisspace.ContainsEgg)
            {
                if (user.Backpack.Count < 11)
                {
                    user.Backpack.Add(thisspace.ThisEgg);
                    thismap = user.GatherEggMap(user, thismap);
                }
            }
            else if (thisspace.MapspaceResources.Count < (11 - user.Backpack.Count))
            {
                foreach (Resource ResourceGather in thisspace.MapspaceResources)
                {
                    if (user.Backpack.Count < 11)
                    {
                        user.Backpack.Add(ResourceGather);
                    }
                }
                thismap = user.GatherResourcesMap(user, thismap);
            }
            return user;
        }
        public Mapspace[,] GatherResourcesMap(Player user, Mapspace[,] thismap)
        {

            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsResource = false;
            thisspace.MapspaceResources.Clear();
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Mapspace[,] GatherMonksMap(Player user, Mapspace[,] thismap)
        {

            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsMonk = false;
            thisspace.ThisMonk = null;
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Mapspace[,] GatherEggMap(Player user, Mapspace[,] thismap)
        {

            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsEgg = false;
            thisspace.ThisEgg = null;
            thismap[i, j] = thisspace;
            return thismap;
        }
        public Mapspace[,] GatherScrollsMap(Player user, Mapspace[,] thismap)
        {

            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            Mapspace thisspace = thismap[i, j];
            thisspace.ContainsScroll = false;
            thisspace.ThisScroll = null;
            thismap[i, j] = thisspace;
            return thismap;
        }

        public Mapspace UpdatePlayerPos(Mapspace thismapspace, Player user)
        {
            thismapspace.ContainsPlayer = true;
            thismapspace.MapspacePlayer = user;
            return thismapspace;
        }
        public string UpdateStatusWindow(Player user)
        {
            string output = $"Positie : ({(user.PlayerpositionXaxis) + 1} , {(user.PlayerpositionYaxis) + 1})\nVictoryPoints: {user.Victorypoints}\nHealthpoints: {user.Healthpoints}\n" +
                $"MaximumAD: {user.MaxActiondiscs} -- Current: {user.CurrentActiondiscs}\nMaxMP: {user.MaxMovepoints} -- Current: {user.CurrentMovepoints}\nDualwielding = {Dualwielding}";
            return output;
        }


    }
}
