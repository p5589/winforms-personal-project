﻿
namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    partial class Frm_Combat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Combat));
            this.btnAbort = new System.Windows.Forms.Button();
            this.btnSlayDragon = new System.Windows.Forms.Button();
            this.txtStatusPlayer = new System.Windows.Forms.RichTextBox();
            this.txtStatusDragon = new System.Windows.Forms.RichTextBox();
            this.picUser = new System.Windows.Forms.PictureBox();
            this.picDragon = new System.Windows.Forms.PictureBox();
            this.txtCombatStatus = new System.Windows.Forms.RichTextBox();
            this.btn00 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button00 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDragon)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAbort
            // 
            this.btnAbort.Location = new System.Drawing.Point(604, 26);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(161, 72);
            this.btnAbort.TabIndex = 0;
            this.btnAbort.Text = "Terug Naar Map";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // btnSlayDragon
            // 
            this.btnSlayDragon.Location = new System.Drawing.Point(226, 94);
            this.btnSlayDragon.Name = "btnSlayDragon";
            this.btnSlayDragon.Size = new System.Drawing.Size(164, 68);
            this.btnSlayDragon.TabIndex = 1;
            this.btnSlayDragon.Text = "Slaydragon";
            this.btnSlayDragon.UseVisualStyleBackColor = true;
            this.btnSlayDragon.Click += new System.EventHandler(this.btnSlayDragon_Click);
            // 
            // txtStatusPlayer
            // 
            this.txtStatusPlayer.Location = new System.Drawing.Point(12, 26);
            this.txtStatusPlayer.Name = "txtStatusPlayer";
            this.txtStatusPlayer.Size = new System.Drawing.Size(205, 91);
            this.txtStatusPlayer.TabIndex = 2;
            this.txtStatusPlayer.Text = "";
            // 
            // txtStatusDragon
            // 
            this.txtStatusDragon.Location = new System.Drawing.Point(792, 26);
            this.txtStatusDragon.Name = "txtStatusDragon";
            this.txtStatusDragon.Size = new System.Drawing.Size(255, 89);
            this.txtStatusDragon.TabIndex = 3;
            this.txtStatusDragon.Text = "";
            // 
            // picUser
            // 
            this.picUser.Location = new System.Drawing.Point(16, 123);
            this.picUser.Name = "picUser";
            this.picUser.Size = new System.Drawing.Size(204, 240);
            this.picUser.TabIndex = 4;
            this.picUser.TabStop = false;
            // 
            // picDragon
            // 
            this.picDragon.Location = new System.Drawing.Point(792, 121);
            this.picDragon.Name = "picDragon";
            this.picDragon.Size = new System.Drawing.Size(245, 228);
            this.picDragon.TabIndex = 5;
            this.picDragon.TabStop = false;
            // 
            // txtCombatStatus
            // 
            this.txtCombatStatus.Location = new System.Drawing.Point(410, 26);
            this.txtCombatStatus.Name = "txtCombatStatus";
            this.txtCombatStatus.Size = new System.Drawing.Size(188, 89);
            this.txtCombatStatus.TabIndex = 6;
            this.txtCombatStatus.Text = "";
            // 
            // btn00
            // 
            this.btn00.Location = new System.Drawing.Point(324, 26);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(66, 61);
            this.btn00.TabIndex = 7;
            this.btn00.Text = "Roll Dice";
            this.btn00.UseVisualStyleBackColor = true;
            this.btn00.Click += new System.EventHandler(this.btnRoll_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Status Player";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(410, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Status Combat";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(792, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Status Dragon";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button00);
            this.groupBox1.Location = new System.Drawing.Point(238, 195);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(498, 233);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // button00
            // 
            this.button00.Image = ((System.Drawing.Image)(resources.GetObject("button00.Image")));
            this.button00.Location = new System.Drawing.Point(17, 26);
            this.button00.Name = "button00";
            this.button00.Size = new System.Drawing.Size(32, 29);
            this.button00.TabIndex = 0;
            this.button00.UseVisualStyleBackColor = true;
            // 
            // Frm_Combat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 514);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn00);
            this.Controls.Add(this.txtCombatStatus);
            this.Controls.Add(this.picDragon);
            this.Controls.Add(this.picUser);
            this.Controls.Add(this.txtStatusDragon);
            this.Controls.Add(this.txtStatusPlayer);
            this.Controls.Add(this.btnSlayDragon);
            this.Controls.Add(this.btnAbort);
            this.Name = "Frm_Combat";
            this.Text = "Frm_Combat";
            this.Load += new System.EventHandler(this.Frm_Combat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDragon)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.Button btnSlayDragon;
        private System.Windows.Forms.RichTextBox txtStatusPlayer;
        private System.Windows.Forms.RichTextBox txtStatusDragon;
        private System.Windows.Forms.PictureBox picUser;
        private System.Windows.Forms.PictureBox picDragon;
        private System.Windows.Forms.RichTextBox txtCombatStatus;
        private System.Windows.Forms.Button btn00;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button00;
    }
}