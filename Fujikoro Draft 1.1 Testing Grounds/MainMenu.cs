﻿using System;
using System.Windows.Forms;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public partial class MainMenu : Form
    {

        public MainMenu()
        {
            InitializeComponent();
        }

        private void btnTestMap_Click(object sender, EventArgs e)
        {
            Frm_TestMap thistestmap = new Frm_TestMap();
            this.AddOwnedForm(thistestmap);
            thistestmap.ShowDialog();
            if (thistestmap.DialogResult == DialogResult.OK)
            {
                thistestmap.Close();
            }

        }

        private void MainMenu_Load(object sender, EventArgs e)
        {
            txtChangelog.Text = $"Welkom Bij FujiKoro Build 1.2\n\nDoel : \nWinforms basis game functies, classes methods, OOP etc.\nHexa Map design";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtChangelog_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnChangeLog_Click(object sender, EventArgs e)
        {
            Frm_ChangeLog thischangelog = new Frm_ChangeLog();
            this.AddOwnedForm(thischangelog);
            thischangelog.ShowDialog();
            if (thischangelog.DialogResult == DialogResult.OK)
            {
                thischangelog.Close();
            }
        }

        private void btnMapDesignJay_Click(object sender, EventArgs e)
        {
            FrmHexaMap thisdesign = new FrmHexaMap();
            this.AddOwnedForm(thisdesign);
            thisdesign.ShowDialog();
            if (thisdesign.DialogResult == DialogResult.OK)
            {
                thisdesign.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Frm_ButtonMap thisdesign = new Frm_ButtonMap();
            this.AddOwnedForm(thisdesign);
            thisdesign.ShowDialog();
            if (thisdesign.DialogResult == DialogResult.OK) ;
            {

                thisdesign.Close();
            }
        }
    }
}
