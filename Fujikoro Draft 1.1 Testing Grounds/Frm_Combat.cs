﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public partial class Frm_Combat : Form
    {
        Player combatplayer = new Player("Model1");
        Dragon combatdragon = new Dragon();
        Random generator = new Random();

        public Frm_Combat(Player user, Dragon thisdragon)
        {
            combatplayer = user;
            combatdragon = thisdragon;
            InitializeComponent();
        }

        private void Frm_Combat_Load(object sender, EventArgs e)
        {
            picUser.Image =
            picDragon.Image = Image.FromFile("alive.jpg");
            picDragon.SizeMode = PictureBoxSizeMode.StretchImage;
            picUser.Image = Image.FromFile("jack.jpg");
            picUser.SizeMode = PictureBoxSizeMode.StretchImage;
            txtStatusDragon.Text = combatdragon.UpdateStatusWindow(combatdragon);
            txtStatusPlayer.Text = combatplayer.UpdateStatusWindow(combatplayer);
            btnAbort.Enabled = false;
            btn00.Enabled = true;
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnSlayDragon_Click(object sender, EventArgs e)
        {
            btnSlayDragon.Enabled = false;
            combatdragon.DragonHealthpoints = 0;
            txtStatusDragon.Text = combatdragon.UpdateStatusWindow(combatdragon);
            combatdragon = null;
            picDragon.Image = Image.FromFile("dead.jpg");
            picDragon.SizeMode = PictureBoxSizeMode.StretchImage;
            btnAbort.Enabled = true;
            txtCombatStatus.Text = $"De Draak is dood\nDrakenei gegenereerd en op grond gesmeten.";
        }

        private void btnRoll_Click(object sender, EventArgs e)
        {
            int x = 0, y = 0;
            string buttonName = "btn" + y.ToString() + x.ToString();
            buttonName = "btn" + y.ToString() + x.ToString();
            Button btn = this.Controls.Find(buttonName, true)[0] as Button;
            btn.BackColor = System.Drawing.Color.Blue;
            btn.Image = Image.FromFile("Wood.jpg");
            btn.Text = "Wood";
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderColor = Color.Red;
            btn.FlatAppearance.BorderSize = 1;


        }

    }
}
