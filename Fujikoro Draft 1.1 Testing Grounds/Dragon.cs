﻿namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public class Dragon
    {
        private string _dragonmodel;
        private int _dragondicecount;
        private int _dragonhealthpoints;
        private int _dragoninitiative;

        public Dragon()
        {

        }

        public Dragon(string dragonmodel, int dicecount, int healthpoints, int initiative)
        {
            _dragonmodel = dragonmodel;
            _dragondicecount = dicecount;
            _dragonhealthpoints = healthpoints;
            _dragoninitiative = initiative;
        }
        public string DragonModel
        {
            get { return _dragonmodel; }
            set { _dragonmodel = value; }
        }
        public int DragonDicecount
        {
            get { return _dragondicecount; }
            set { _dragondicecount = value; }
        }
        public int DragonHealthpoints
        {
            get { return _dragonhealthpoints; }
            set { _dragonhealthpoints = value; }
        }
        public int DragonInitiative
        {
            get { return _dragoninitiative; }
            set { _dragoninitiative = value; }
        }
        public override string ToString()
        {
            return $"Model: {DragonModel}-Dice: {DragonDicecount}-Health: {DragonHealthpoints}-Initiative: {DragonInitiative}";
        }
        public string UpdateStatusWindow(Dragon thisdragon)
        {
            string output = $"Model: {thisdragon.DragonModel}\nDice: {thisdragon.DragonDicecount}\nHealth : {thisdragon.DragonHealthpoints}\nInitiative: {thisdragon.DragonInitiative}";
            return output;
        }



    }
}
