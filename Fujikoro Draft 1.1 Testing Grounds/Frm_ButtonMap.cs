﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public partial class Frm_ButtonMap : Form
    {
        Button[,] arrMapButtons = new Button[14, 14];
        Mapspace[,] arrMapspaces = new Mapspace[14, 14];
        public Frm_ButtonMap()
        {
            InitializeComponent();
            for (int i = 0; i < 14; i++)
            {
                for (int j = 0; j < 14; j++)
                {

                    Mapspace thismapspace = new Mapspace();
                    arrMapspaces[i, j] = thismapspace;
                }
            }
            Mapspace[] starttile = new Mapspace[7];
            for (int i = 0; i < 7; i++)
            {
                Mapspace mymapspace = new Mapspace();
                starttile[i] = mymapspace;
            }
            int x = 0;
            int y = 0;
            int z = 0;
            int posX = 0;
            int posY = 0;
            for (int i = 0; i < 14; i++)
            {
                posX = 0;
                z = 0;
                for (int j = 0; j < 14; j++)
                {
                    string buttonName = "button" + (x + 1).ToString();
                    Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                    btn.Name = "btn" + y.ToString() + "," + z.ToString();
                    btn.FlatStyle = FlatStyle.Flat;
                    btn.FlatAppearance.BorderColor = Color.Red;
                    btn.FlatAppearance.BorderSize = 1;
                    btn.Text = $"{y},{z}";
                    PointF[] points = Polygon(new PointF(30, 30), 6, 30, 0);
                    GraphicsPath polygon_path = new GraphicsPath();
                    polygon_path.AddPolygon(points);
                    Region polygon_region = new Region(polygon_path);
                    btn.Region = polygon_region;


                    if ((i == 0) && (j == 0))
                    {
                        btn.Location = new Point((0), (0));
                    }
                    else if ((i % 2) == 0)
                    {
                        btn.Location = new Point(posX, posY);
                    }
                    else
                    {
                        btn.Location = new Point(posX + 30, posY);

                    }
                    this.Controls.Add(btn);
                    btn.MouseHover += (sender, args) =>
                    {
                        string x = btn.Name;
                        x = x.Remove(0, 3);
                        string[] waarden = (x.Split(','));
                        int a = Convert.ToInt32(waarden[0]);
                        int b = Convert.ToInt32(waarden[1]);
                        txtDebug.Text = arrMapspaces[a, b].MapspaceStatus(arrMapspaces[a, b]);

                    };
                    btn.Click += (sender, args) =>
                    {


                        //Move functie adden als move button enabled is!!
                        //explore functie als enabled en juist!!
                    };
                    if (((i == 5) && (j == 6)) || ((i == 5) && (j == 7)) || ((i == 6) && (j == 6)) || ((i == 6) && (j == 8)) || ((i == 7) && (j == 6)) || ((i == 7) && (j == 7)))
                    {
                        btn.Image = Image.FromFile("Empty.jpg");
                    }
                    else if ((i == 6) && (j == 7))
                    {
                        btn.Image = Image.FromFile("Touw.jpg");
                        Mapspace thismapspace = arrMapspaces[6, 7];
                        thismapspace.PossibleExplore = false;
                        arrMapspaces[6, 7] = thismapspace;
                    }
                    arrMapButtons[i, j] = btn;

                    x++;
                    z++;
                    posX = posX + 60;
                }
                y++;
                posY = posY + 45;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private Button PointtoButton(int x, int y)
        {
            Button btn = new Button();
            foreach (Button thisbutton in arrMapButtons)
            {
                for (int i = (x - 15); i < (x + 15); i++)
                {
                    for (int j = (y - 15); j < (y + 15); j++)
                    {
                        Point thispoint = new Point(x, y);
                        if (thisbutton.Location == thispoint)
                        {
                            btn = thisbutton;
                            break;
                        }
                    }
                }
            }
            return btn;
        }

        private void HoverMap_MouseMove(object sender, MouseEventArgs e)
        {
            int x = Convert.ToInt32(e.X);
            int y = Convert.ToInt32(e.Y);
            Button btn = PointtoButton(x, y);
            btn = this.Controls.Find(btn.Name, true)[0] as Button;
            string buttonName = btn.Name;
            txtDebug.Text = buttonName;
            buttonName.Remove(0, 3);
            string[] theseXY = buttonName.Split(",");
            x = Convert.ToInt32(theseXY[0]);
            y = Convert.ToInt32(theseXY[1]);
            //Nu linken aan mapspace array en dan doorschrijven naar tekstbox








        }
        PointF[] Polygon(PointF center, int count, float radius, float angle)
        {
            PointF[] pts = new PointF[6];
            int counter = 0;
            for (float i = angle; i < 360; i += 360f / count)
            {
                float rad = (float)(Math.PI / 180 * i);
                pts[counter] = (new PointF(center.X + (float)Math.Sin(rad) * radius,
                                    center.Y + (float)Math.Cos(rad) * radius));
                counter++;
            }
            return pts;
        }




    }
}
