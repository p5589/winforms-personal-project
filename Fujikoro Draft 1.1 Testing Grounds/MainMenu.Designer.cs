﻿
namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    partial class MainMenu
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestMap = new System.Windows.Forms.Button();
            this.txtChangelog = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnChangeLog = new System.Windows.Forms.Button();
            this.btnMapDesignJay = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTestMap
            // 
            this.btnTestMap.Location = new System.Drawing.Point(60, 295);
            this.btnTestMap.Name = "btnTestMap";
            this.btnTestMap.Size = new System.Drawing.Size(255, 41);
            this.btnTestMap.TabIndex = 0;
            this.btnTestMap.Text = "Go To Testmap";
            this.btnTestMap.UseVisualStyleBackColor = true;
            this.btnTestMap.Click += new System.EventHandler(this.btnTestMap_Click);
            // 
            // txtChangelog
            // 
            this.txtChangelog.Location = new System.Drawing.Point(60, 35);
            this.txtChangelog.Name = "txtChangelog";
            this.txtChangelog.Size = new System.Drawing.Size(678, 236);
            this.txtChangelog.TabIndex = 1;
            this.txtChangelog.Text = "";
            this.txtChangelog.TextChanged += new System.EventHandler(this.txtChangelog_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Last added Changes";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(60, 370);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(255, 41);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Close App";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnChangeLog
            // 
            this.btnChangeLog.Location = new System.Drawing.Point(362, 295);
            this.btnChangeLog.Name = "btnChangeLog";
            this.btnChangeLog.Size = new System.Drawing.Size(179, 47);
            this.btnChangeLog.TabIndex = 4;
            this.btnChangeLog.Text = "Changelog";
            this.btnChangeLog.UseVisualStyleBackColor = true;
            this.btnChangeLog.Click += new System.EventHandler(this.btnChangeLog_Click);
            // 
            // btnMapDesignJay
            // 
            this.btnMapDesignJay.Location = new System.Drawing.Point(585, 295);
            this.btnMapDesignJay.Name = "btnMapDesignJay";
            this.btnMapDesignJay.Size = new System.Drawing.Size(176, 46);
            this.btnMapDesignJay.TabIndex = 5;
            this.btnMapDesignJay.Text = "MapDesignJay";
            this.btnMapDesignJay.UseVisualStyleBackColor = true;
            this.btnMapDesignJay.Click += new System.EventHandler(this.btnMapDesignJay_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(497, 370);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(264, 50);
            this.button1.TabIndex = 6;
            this.button1.Text = "ToHexaMap BUGGY";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnMapDesignJay);
            this.Controls.Add(this.btnChangeLog);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtChangelog);
            this.Controls.Add(this.btnTestMap);
            this.Name = "MainMenu";
            this.Text = "MainMenu";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTestMap;
        private System.Windows.Forms.RichTextBox txtChangelog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnChangeLog;
        private System.Windows.Forms.Button btnMapDesignJay;
        private System.Windows.Forms.Button button1;
    }
}

