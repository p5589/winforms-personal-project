﻿
namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    partial class Frm_TestMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtMap = new System.Windows.Forms.RichTextBox();
            this.txtStatusPlayer = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGather = new System.Windows.Forms.Button();
            this.btnRest = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.btnEndRound = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnMoveRight = new System.Windows.Forms.Button();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnMoveLeft = new System.Windows.Forms.Button();
            this.cmbPlayers = new System.Windows.Forms.ComboBox();
            this.cmbDragons = new System.Windows.Forms.ComboBox();
            this.cmbScrolls = new System.Windows.Forms.ComboBox();
            this.cmbResources = new System.Windows.Forms.ComboBox();
            this.cmbMonks = new System.Windows.Forms.ComboBox();
            this.txtSpaceInfo = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblRoundCounter = new System.Windows.Forms.Label();
            this.Backpack = new System.Windows.Forms.Label();
            this.lbBackpack = new System.Windows.Forms.ListBox();
            this.btnEmptySelectionBackpack = new System.Windows.Forms.Button();
            this.btnEndSession = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.picBackpackMonk = new System.Windows.Forms.PictureBox();
            this.picSacredMonk = new System.Windows.Forms.PictureBox();
            this.picActionMonk = new System.Windows.Forms.PictureBox();
            this.picWeaponMonk = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.cmbMonkField = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pic48 = new System.Windows.Forms.PictureBox();
            this.pic47 = new System.Windows.Forms.PictureBox();
            this.pic46 = new System.Windows.Forms.PictureBox();
            this.pic45 = new System.Windows.Forms.PictureBox();
            this.pic44 = new System.Windows.Forms.PictureBox();
            this.pic43 = new System.Windows.Forms.PictureBox();
            this.pic42 = new System.Windows.Forms.PictureBox();
            this.pic41 = new System.Windows.Forms.PictureBox();
            this.pic40 = new System.Windows.Forms.PictureBox();
            this.pic38 = new System.Windows.Forms.PictureBox();
            this.pic37 = new System.Windows.Forms.PictureBox();
            this.pic36 = new System.Windows.Forms.PictureBox();
            this.pic35 = new System.Windows.Forms.PictureBox();
            this.pic34 = new System.Windows.Forms.PictureBox();
            this.pic33 = new System.Windows.Forms.PictureBox();
            this.pic32 = new System.Windows.Forms.PictureBox();
            this.pic31 = new System.Windows.Forms.PictureBox();
            this.pic30 = new System.Windows.Forms.PictureBox();
            this.pic28 = new System.Windows.Forms.PictureBox();
            this.pic27 = new System.Windows.Forms.PictureBox();
            this.pic26 = new System.Windows.Forms.PictureBox();
            this.pic25 = new System.Windows.Forms.PictureBox();
            this.pic24 = new System.Windows.Forms.PictureBox();
            this.pic23 = new System.Windows.Forms.PictureBox();
            this.pic22 = new System.Windows.Forms.PictureBox();
            this.pic21 = new System.Windows.Forms.PictureBox();
            this.pic20 = new System.Windows.Forms.PictureBox();
            this.pic18 = new System.Windows.Forms.PictureBox();
            this.pic17 = new System.Windows.Forms.PictureBox();
            this.pic16 = new System.Windows.Forms.PictureBox();
            this.pic15 = new System.Windows.Forms.PictureBox();
            this.pic14 = new System.Windows.Forms.PictureBox();
            this.pic13 = new System.Windows.Forms.PictureBox();
            this.pic12 = new System.Windows.Forms.PictureBox();
            this.pic11 = new System.Windows.Forms.PictureBox();
            this.pic10 = new System.Windows.Forms.PictureBox();
            this.pic08 = new System.Windows.Forms.PictureBox();
            this.pic07 = new System.Windows.Forms.PictureBox();
            this.pic06 = new System.Windows.Forms.PictureBox();
            this.pic05 = new System.Windows.Forms.PictureBox();
            this.pic04 = new System.Windows.Forms.PictureBox();
            this.pic03 = new System.Windows.Forms.PictureBox();
            this.pic02 = new System.Windows.Forms.PictureBox();
            this.pic01 = new System.Windows.Forms.PictureBox();
            this.pic00 = new System.Windows.Forms.PictureBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picBackpackMonk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSacredMonk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picActionMonk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWeaponMonk)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMap
            // 
            this.txtMap.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtMap.Location = new System.Drawing.Point(58, 12);
            this.txtMap.Name = "txtMap";
            this.txtMap.Size = new System.Drawing.Size(1429, 313);
            this.txtMap.TabIndex = 0;
            this.txtMap.Text = "";
            // 
            // txtStatusPlayer
            // 
            this.txtStatusPlayer.Location = new System.Drawing.Point(895, 673);
            this.txtStatusPlayer.Name = "txtStatusPlayer";
            this.txtStatusPlayer.Size = new System.Drawing.Size(239, 132);
            this.txtStatusPlayer.TabIndex = 1;
            this.txtStatusPlayer.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(895, 650);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Status:";
            // 
            // btnGather
            // 
            this.btnGather.Location = new System.Drawing.Point(275, 423);
            this.btnGather.Name = "btnGather";
            this.btnGather.Size = new System.Drawing.Size(170, 49);
            this.btnGather.TabIndex = 3;
            this.btnGather.Text = "Gather";
            this.btnGather.UseVisualStyleBackColor = true;
            this.btnGather.Click += new System.EventHandler(this.btnGather_Click);
            // 
            // btnRest
            // 
            this.btnRest.Location = new System.Drawing.Point(275, 359);
            this.btnRest.Name = "btnRest";
            this.btnRest.Size = new System.Drawing.Size(170, 49);
            this.btnRest.TabIndex = 4;
            this.btnRest.Text = "Rest/Craft";
            this.btnRest.UseVisualStyleBackColor = true;
            this.btnRest.Click += new System.EventHandler(this.btnRest_Click);
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(275, 492);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(170, 49);
            this.btnMove.TabIndex = 5;
            this.btnMove.Text = "Move (Removes AD)";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnEndRound
            // 
            this.btnEndRound.Location = new System.Drawing.Point(1, 331);
            this.btnEndRound.Name = "btnEndRound";
            this.btnEndRound.Size = new System.Drawing.Size(170, 49);
            this.btnEndRound.TabIndex = 6;
            this.btnEndRound.Text = "EndRound";
            this.btnEndRound.UseVisualStyleBackColor = true;
            this.btnEndRound.Click += new System.EventHandler(this.btnEndRound_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Location = new System.Drawing.Point(157, 399);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(55, 34);
            this.btnMoveUp.TabIndex = 7;
            this.btnMoveUp.Text = "Up";
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnMoveRight
            // 
            this.btnMoveRight.Location = new System.Drawing.Point(209, 431);
            this.btnMoveRight.Name = "btnMoveRight";
            this.btnMoveRight.Size = new System.Drawing.Size(59, 33);
            this.btnMoveRight.TabIndex = 8;
            this.btnMoveRight.Text = "Right";
            this.btnMoveRight.UseVisualStyleBackColor = true;
            this.btnMoveRight.Click += new System.EventHandler(this.btnMoveRight_Click);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Location = new System.Drawing.Point(157, 470);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(60, 35);
            this.btnMoveDown.TabIndex = 9;
            this.btnMoveDown.Text = "Down";
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnMoveLeft
            // 
            this.btnMoveLeft.Location = new System.Drawing.Point(99, 431);
            this.btnMoveLeft.Name = "btnMoveLeft";
            this.btnMoveLeft.Size = new System.Drawing.Size(61, 33);
            this.btnMoveLeft.TabIndex = 10;
            this.btnMoveLeft.Text = "Left";
            this.btnMoveLeft.UseVisualStyleBackColor = true;
            this.btnMoveLeft.Click += new System.EventHandler(this.btnMoveLeft_Click);
            // 
            // cmbPlayers
            // 
            this.cmbPlayers.FormattingEnabled = true;
            this.cmbPlayers.Location = new System.Drawing.Point(1024, 410);
            this.cmbPlayers.Name = "cmbPlayers";
            this.cmbPlayers.Size = new System.Drawing.Size(303, 28);
            this.cmbPlayers.TabIndex = 11;
            // 
            // cmbDragons
            // 
            this.cmbDragons.FormattingEnabled = true;
            this.cmbDragons.Location = new System.Drawing.Point(1024, 444);
            this.cmbDragons.Name = "cmbDragons";
            this.cmbDragons.Size = new System.Drawing.Size(303, 28);
            this.cmbDragons.TabIndex = 12;
            // 
            // cmbScrolls
            // 
            this.cmbScrolls.FormattingEnabled = true;
            this.cmbScrolls.Location = new System.Drawing.Point(1022, 478);
            this.cmbScrolls.Name = "cmbScrolls";
            this.cmbScrolls.Size = new System.Drawing.Size(305, 28);
            this.cmbScrolls.TabIndex = 13;
            // 
            // cmbResources
            // 
            this.cmbResources.FormattingEnabled = true;
            this.cmbResources.Location = new System.Drawing.Point(1022, 517);
            this.cmbResources.Name = "cmbResources";
            this.cmbResources.Size = new System.Drawing.Size(303, 28);
            this.cmbResources.TabIndex = 14;
            // 
            // cmbMonks
            // 
            this.cmbMonks.FormattingEnabled = true;
            this.cmbMonks.Location = new System.Drawing.Point(1024, 551);
            this.cmbMonks.Name = "cmbMonks";
            this.cmbMonks.Size = new System.Drawing.Size(303, 28);
            this.cmbMonks.TabIndex = 15;
            // 
            // txtSpaceInfo
            // 
            this.txtSpaceInfo.Location = new System.Drawing.Point(1184, 632);
            this.txtSpaceInfo.Name = "txtSpaceInfo";
            this.txtSpaceInfo.Size = new System.Drawing.Size(143, 138);
            this.txtSpaceInfo.TabIndex = 16;
            this.txtSpaceInfo.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1184, 783);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "CurrentSpaceInfo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(278, 328);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "Ronde";
            // 
            // lblRoundCounter
            // 
            this.lblRoundCounter.AutoSize = true;
            this.lblRoundCounter.Location = new System.Drawing.Point(342, 328);
            this.lblRoundCounter.Name = "lblRoundCounter";
            this.lblRoundCounter.Size = new System.Drawing.Size(21, 20);
            this.lblRoundCounter.TabIndex = 19;
            this.lblRoundCounter.Text = "--";
            // 
            // Backpack
            // 
            this.Backpack.AutoSize = true;
            this.Backpack.Location = new System.Drawing.Point(676, 344);
            this.Backpack.Name = "Backpack";
            this.Backpack.Size = new System.Drawing.Size(71, 20);
            this.Backpack.TabIndex = 20;
            this.Backpack.Text = "Backpack";
            // 
            // lbBackpack
            // 
            this.lbBackpack.FormattingEnabled = true;
            this.lbBackpack.ItemHeight = 20;
            this.lbBackpack.Location = new System.Drawing.Point(764, 344);
            this.lbBackpack.Name = "lbBackpack";
            this.lbBackpack.Size = new System.Drawing.Size(252, 224);
            this.lbBackpack.TabIndex = 21;
            // 
            // btnEmptySelectionBackpack
            // 
            this.btnEmptySelectionBackpack.Location = new System.Drawing.Point(1022, 344);
            this.btnEmptySelectionBackpack.Name = "btnEmptySelectionBackpack";
            this.btnEmptySelectionBackpack.Size = new System.Drawing.Size(192, 54);
            this.btnEmptySelectionBackpack.TabIndex = 22;
            this.btnEmptySelectionBackpack.Text = "Drop Selectie uit Pack";
            this.btnEmptySelectionBackpack.UseVisualStyleBackColor = true;
            this.btnEmptySelectionBackpack.Click += new System.EventHandler(this.btnEmptySelectionBackpack_Click);
            // 
            // btnEndSession
            // 
            this.btnEndSession.Location = new System.Drawing.Point(1171, 585);
            this.btnEndSession.Name = "btnEndSession";
            this.btnEndSession.Size = new System.Drawing.Size(156, 41);
            this.btnEndSession.TabIndex = 23;
            this.btnEndSession.Text = "End Test";
            this.btnEndSession.UseVisualStyleBackColor = true;
            this.btnEndSession.Click += new System.EventHandler(this.btnEndSession_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(721, 675);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(149, 129);
            this.richTextBox1.TabIndex = 26;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(866, 577);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(129, 58);
            this.richTextBox2.TabIndex = 27;
            this.richTextBox2.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(764, 650);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "Helmet:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(786, 595);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 20);
            this.label7.TabIndex = 30;
            this.label7.Text = "Sandals:";
            // 
            // picBackpackMonk
            // 
            this.picBackpackMonk.Location = new System.Drawing.Point(6, 29);
            this.picBackpackMonk.Name = "picBackpackMonk";
            this.picBackpackMonk.Size = new System.Drawing.Size(81, 48);
            this.picBackpackMonk.TabIndex = 31;
            this.picBackpackMonk.TabStop = false;
            // 
            // picSacredMonk
            // 
            this.picSacredMonk.Location = new System.Drawing.Point(147, 26);
            this.picSacredMonk.Name = "picSacredMonk";
            this.picSacredMonk.Size = new System.Drawing.Size(81, 48);
            this.picSacredMonk.TabIndex = 32;
            this.picSacredMonk.TabStop = false;
            // 
            // picActionMonk
            // 
            this.picActionMonk.Location = new System.Drawing.Point(6, 103);
            this.picActionMonk.Name = "picActionMonk";
            this.picActionMonk.Size = new System.Drawing.Size(81, 48);
            this.picActionMonk.TabIndex = 33;
            this.picActionMonk.TabStop = false;
            // 
            // picWeaponMonk
            // 
            this.picWeaponMonk.Location = new System.Drawing.Point(147, 103);
            this.picWeaponMonk.Name = "picWeaponMonk";
            this.picWeaponMonk.Size = new System.Drawing.Size(81, 48);
            this.picWeaponMonk.TabIndex = 34;
            this.picWeaponMonk.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.picBackpackMonk);
            this.groupBox1.Controls.Add(this.picWeaponMonk);
            this.groupBox1.Controls.Add(this.picSacredMonk);
            this.groupBox1.Controls.Add(this.picActionMonk);
            this.groupBox1.Location = new System.Drawing.Point(481, 491);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 166);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Monk Field";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Location = new System.Drawing.Point(467, 675);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 131);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scroll Field";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new System.Drawing.Point(132, 32);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(88, 82);
            this.pictureBox6.TabIndex = 1;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(18, 31);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(88, 82);
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // cmbMonkField
            // 
            this.cmbMonkField.FormattingEnabled = true;
            this.cmbMonkField.Location = new System.Drawing.Point(479, 444);
            this.cmbMonkField.Name = "cmbMonkField";
            this.cmbMonkField.Size = new System.Drawing.Size(245, 28);
            this.cmbMonkField.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(479, 413);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(195, 20);
            this.label6.TabIndex = 38;
            this.label6.Text = "Monk Field Selection Gather";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pic48);
            this.groupBox3.Controls.Add(this.pic47);
            this.groupBox3.Controls.Add(this.pic46);
            this.groupBox3.Controls.Add(this.pic45);
            this.groupBox3.Controls.Add(this.pic44);
            this.groupBox3.Controls.Add(this.pic43);
            this.groupBox3.Controls.Add(this.pic42);
            this.groupBox3.Controls.Add(this.pic41);
            this.groupBox3.Controls.Add(this.pic40);
            this.groupBox3.Controls.Add(this.pic38);
            this.groupBox3.Controls.Add(this.pic37);
            this.groupBox3.Controls.Add(this.pic36);
            this.groupBox3.Controls.Add(this.pic35);
            this.groupBox3.Controls.Add(this.pic34);
            this.groupBox3.Controls.Add(this.pic33);
            this.groupBox3.Controls.Add(this.pic32);
            this.groupBox3.Controls.Add(this.pic31);
            this.groupBox3.Controls.Add(this.pic30);
            this.groupBox3.Controls.Add(this.pic28);
            this.groupBox3.Controls.Add(this.pic27);
            this.groupBox3.Controls.Add(this.pic26);
            this.groupBox3.Controls.Add(this.pic25);
            this.groupBox3.Controls.Add(this.pic24);
            this.groupBox3.Controls.Add(this.pic23);
            this.groupBox3.Controls.Add(this.pic22);
            this.groupBox3.Controls.Add(this.pic21);
            this.groupBox3.Controls.Add(this.pic20);
            this.groupBox3.Controls.Add(this.pic18);
            this.groupBox3.Controls.Add(this.pic17);
            this.groupBox3.Controls.Add(this.pic16);
            this.groupBox3.Controls.Add(this.pic15);
            this.groupBox3.Controls.Add(this.pic14);
            this.groupBox3.Controls.Add(this.pic13);
            this.groupBox3.Controls.Add(this.pic12);
            this.groupBox3.Controls.Add(this.pic11);
            this.groupBox3.Controls.Add(this.pic10);
            this.groupBox3.Controls.Add(this.pic08);
            this.groupBox3.Controls.Add(this.pic07);
            this.groupBox3.Controls.Add(this.pic06);
            this.groupBox3.Controls.Add(this.pic05);
            this.groupBox3.Controls.Add(this.pic04);
            this.groupBox3.Controls.Add(this.pic03);
            this.groupBox3.Controls.Add(this.pic02);
            this.groupBox3.Controls.Add(this.pic01);
            this.groupBox3.Controls.Add(this.pic00);
            this.groupBox3.Location = new System.Drawing.Point(31, 551);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(414, 279);
            this.groupBox3.TabIndex = 39;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // pic48
            // 
            this.pic48.Location = new System.Drawing.Point(354, 212);
            this.pic48.Name = "pic48";
            this.pic48.Size = new System.Drawing.Size(37, 40);
            this.pic48.TabIndex = 44;
            this.pic48.TabStop = false;
            // 
            // pic47
            // 
            this.pic47.Location = new System.Drawing.Point(307, 212);
            this.pic47.Name = "pic47";
            this.pic47.Size = new System.Drawing.Size(37, 40);
            this.pic47.TabIndex = 43;
            this.pic47.TabStop = false;
            // 
            // pic46
            // 
            this.pic46.Location = new System.Drawing.Point(264, 212);
            this.pic46.Name = "pic46";
            this.pic46.Size = new System.Drawing.Size(37, 40);
            this.pic46.TabIndex = 42;
            this.pic46.TabStop = false;
            // 
            // pic45
            // 
            this.pic45.Location = new System.Drawing.Point(221, 212);
            this.pic45.Name = "pic45";
            this.pic45.Size = new System.Drawing.Size(37, 40);
            this.pic45.TabIndex = 41;
            this.pic45.TabStop = false;
            // 
            // pic44
            // 
            this.pic44.Location = new System.Drawing.Point(178, 212);
            this.pic44.Name = "pic44";
            this.pic44.Size = new System.Drawing.Size(37, 40);
            this.pic44.TabIndex = 40;
            this.pic44.TabStop = false;
            // 
            // pic43
            // 
            this.pic43.Location = new System.Drawing.Point(135, 212);
            this.pic43.Name = "pic43";
            this.pic43.Size = new System.Drawing.Size(37, 40);
            this.pic43.TabIndex = 39;
            this.pic43.TabStop = false;
            // 
            // pic42
            // 
            this.pic42.Location = new System.Drawing.Point(92, 212);
            this.pic42.Name = "pic42";
            this.pic42.Size = new System.Drawing.Size(37, 40);
            this.pic42.TabIndex = 38;
            this.pic42.TabStop = false;
            // 
            // pic41
            // 
            this.pic41.Location = new System.Drawing.Point(49, 212);
            this.pic41.Name = "pic41";
            this.pic41.Size = new System.Drawing.Size(37, 40);
            this.pic41.TabIndex = 37;
            this.pic41.TabStop = false;
            // 
            // pic40
            // 
            this.pic40.Location = new System.Drawing.Point(6, 212);
            this.pic40.Name = "pic40";
            this.pic40.Size = new System.Drawing.Size(37, 40);
            this.pic40.TabIndex = 36;
            this.pic40.TabStop = false;
            // 
            // pic38
            // 
            this.pic38.Location = new System.Drawing.Point(354, 166);
            this.pic38.Name = "pic38";
            this.pic38.Size = new System.Drawing.Size(37, 40);
            this.pic38.TabIndex = 35;
            this.pic38.TabStop = false;
            // 
            // pic37
            // 
            this.pic37.Location = new System.Drawing.Point(311, 166);
            this.pic37.Name = "pic37";
            this.pic37.Size = new System.Drawing.Size(37, 40);
            this.pic37.TabIndex = 34;
            this.pic37.TabStop = false;
            // 
            // pic36
            // 
            this.pic36.Location = new System.Drawing.Point(264, 166);
            this.pic36.Name = "pic36";
            this.pic36.Size = new System.Drawing.Size(37, 40);
            this.pic36.TabIndex = 33;
            this.pic36.TabStop = false;
            // 
            // pic35
            // 
            this.pic35.Location = new System.Drawing.Point(221, 166);
            this.pic35.Name = "pic35";
            this.pic35.Size = new System.Drawing.Size(37, 40);
            this.pic35.TabIndex = 32;
            this.pic35.TabStop = false;
            // 
            // pic34
            // 
            this.pic34.Location = new System.Drawing.Point(178, 166);
            this.pic34.Name = "pic34";
            this.pic34.Size = new System.Drawing.Size(37, 40);
            this.pic34.TabIndex = 31;
            this.pic34.TabStop = false;
            // 
            // pic33
            // 
            this.pic33.Location = new System.Drawing.Point(135, 166);
            this.pic33.Name = "pic33";
            this.pic33.Size = new System.Drawing.Size(37, 40);
            this.pic33.TabIndex = 30;
            this.pic33.TabStop = false;
            // 
            // pic32
            // 
            this.pic32.Location = new System.Drawing.Point(92, 166);
            this.pic32.Name = "pic32";
            this.pic32.Size = new System.Drawing.Size(37, 40);
            this.pic32.TabIndex = 29;
            this.pic32.TabStop = false;
            // 
            // pic31
            // 
            this.pic31.Location = new System.Drawing.Point(49, 166);
            this.pic31.Name = "pic31";
            this.pic31.Size = new System.Drawing.Size(37, 40);
            this.pic31.TabIndex = 28;
            this.pic31.TabStop = false;
            // 
            // pic30
            // 
            this.pic30.Location = new System.Drawing.Point(6, 166);
            this.pic30.Name = "pic30";
            this.pic30.Size = new System.Drawing.Size(37, 40);
            this.pic30.TabIndex = 27;
            this.pic30.TabStop = false;
            // 
            // pic28
            // 
            this.pic28.Location = new System.Drawing.Point(354, 120);
            this.pic28.Name = "pic28";
            this.pic28.Size = new System.Drawing.Size(37, 40);
            this.pic28.TabIndex = 26;
            this.pic28.TabStop = false;
            // 
            // pic27
            // 
            this.pic27.Location = new System.Drawing.Point(311, 120);
            this.pic27.Name = "pic27";
            this.pic27.Size = new System.Drawing.Size(37, 40);
            this.pic27.TabIndex = 25;
            this.pic27.TabStop = false;
            // 
            // pic26
            // 
            this.pic26.Location = new System.Drawing.Point(268, 120);
            this.pic26.Name = "pic26";
            this.pic26.Size = new System.Drawing.Size(37, 40);
            this.pic26.TabIndex = 24;
            this.pic26.TabStop = false;
            // 
            // pic25
            // 
            this.pic25.Location = new System.Drawing.Point(221, 120);
            this.pic25.Name = "pic25";
            this.pic25.Size = new System.Drawing.Size(37, 40);
            this.pic25.TabIndex = 23;
            this.pic25.TabStop = false;
            // 
            // pic24
            // 
            this.pic24.Location = new System.Drawing.Point(178, 120);
            this.pic24.Name = "pic24";
            this.pic24.Size = new System.Drawing.Size(37, 40);
            this.pic24.TabIndex = 22;
            this.pic24.TabStop = false;
            // 
            // pic23
            // 
            this.pic23.Location = new System.Drawing.Point(135, 120);
            this.pic23.Name = "pic23";
            this.pic23.Size = new System.Drawing.Size(37, 40);
            this.pic23.TabIndex = 21;
            this.pic23.TabStop = false;
            // 
            // pic22
            // 
            this.pic22.Location = new System.Drawing.Point(92, 120);
            this.pic22.Name = "pic22";
            this.pic22.Size = new System.Drawing.Size(37, 40);
            this.pic22.TabIndex = 20;
            this.pic22.TabStop = false;
            // 
            // pic21
            // 
            this.pic21.Location = new System.Drawing.Point(49, 120);
            this.pic21.Name = "pic21";
            this.pic21.Size = new System.Drawing.Size(37, 40);
            this.pic21.TabIndex = 19;
            this.pic21.TabStop = false;
            // 
            // pic20
            // 
            this.pic20.Location = new System.Drawing.Point(6, 120);
            this.pic20.Name = "pic20";
            this.pic20.Size = new System.Drawing.Size(37, 40);
            this.pic20.TabIndex = 18;
            this.pic20.TabStop = false;
            // 
            // pic18
            // 
            this.pic18.Location = new System.Drawing.Point(354, 72);
            this.pic18.Name = "pic18";
            this.pic18.Size = new System.Drawing.Size(37, 40);
            this.pic18.TabIndex = 17;
            this.pic18.TabStop = false;
            // 
            // pic17
            // 
            this.pic17.Location = new System.Drawing.Point(311, 71);
            this.pic17.Name = "pic17";
            this.pic17.Size = new System.Drawing.Size(37, 40);
            this.pic17.TabIndex = 16;
            this.pic17.TabStop = false;
            // 
            // pic16
            // 
            this.pic16.Location = new System.Drawing.Point(268, 71);
            this.pic16.Name = "pic16";
            this.pic16.Size = new System.Drawing.Size(37, 40);
            this.pic16.TabIndex = 15;
            this.pic16.TabStop = false;
            // 
            // pic15
            // 
            this.pic15.Location = new System.Drawing.Point(221, 71);
            this.pic15.Name = "pic15";
            this.pic15.Size = new System.Drawing.Size(37, 40);
            this.pic15.TabIndex = 14;
            this.pic15.TabStop = false;
            // 
            // pic14
            // 
            this.pic14.Location = new System.Drawing.Point(178, 71);
            this.pic14.Name = "pic14";
            this.pic14.Size = new System.Drawing.Size(37, 40);
            this.pic14.TabIndex = 13;
            this.pic14.TabStop = false;
            // 
            // pic13
            // 
            this.pic13.Location = new System.Drawing.Point(135, 71);
            this.pic13.Name = "pic13";
            this.pic13.Size = new System.Drawing.Size(37, 40);
            this.pic13.TabIndex = 12;
            this.pic13.TabStop = false;
            // 
            // pic12
            // 
            this.pic12.Location = new System.Drawing.Point(92, 71);
            this.pic12.Name = "pic12";
            this.pic12.Size = new System.Drawing.Size(37, 40);
            this.pic12.TabIndex = 11;
            this.pic12.TabStop = false;
            // 
            // pic11
            // 
            this.pic11.Location = new System.Drawing.Point(49, 71);
            this.pic11.Name = "pic11";
            this.pic11.Size = new System.Drawing.Size(37, 40);
            this.pic11.TabIndex = 10;
            this.pic11.TabStop = false;
            // 
            // pic10
            // 
            this.pic10.Location = new System.Drawing.Point(6, 71);
            this.pic10.Name = "pic10";
            this.pic10.Size = new System.Drawing.Size(37, 40);
            this.pic10.TabIndex = 9;
            this.pic10.TabStop = false;
            // 
            // pic08
            // 
            this.pic08.Location = new System.Drawing.Point(354, 26);
            this.pic08.Name = "pic08";
            this.pic08.Size = new System.Drawing.Size(37, 40);
            this.pic08.TabIndex = 8;
            this.pic08.TabStop = false;
            // 
            // pic07
            // 
            this.pic07.Location = new System.Drawing.Point(311, 26);
            this.pic07.Name = "pic07";
            this.pic07.Size = new System.Drawing.Size(37, 40);
            this.pic07.TabIndex = 7;
            this.pic07.TabStop = false;
            // 
            // pic06
            // 
            this.pic06.Location = new System.Drawing.Point(268, 26);
            this.pic06.Name = "pic06";
            this.pic06.Size = new System.Drawing.Size(37, 40);
            this.pic06.TabIndex = 6;
            this.pic06.TabStop = false;
            // 
            // pic05
            // 
            this.pic05.Location = new System.Drawing.Point(221, 26);
            this.pic05.Name = "pic05";
            this.pic05.Size = new System.Drawing.Size(37, 40);
            this.pic05.TabIndex = 5;
            this.pic05.TabStop = false;
            // 
            // pic04
            // 
            this.pic04.Location = new System.Drawing.Point(178, 26);
            this.pic04.Name = "pic04";
            this.pic04.Size = new System.Drawing.Size(37, 40);
            this.pic04.TabIndex = 4;
            this.pic04.TabStop = false;
            // 
            // pic03
            // 
            this.pic03.Location = new System.Drawing.Point(135, 26);
            this.pic03.Name = "pic03";
            this.pic03.Size = new System.Drawing.Size(37, 40);
            this.pic03.TabIndex = 3;
            this.pic03.TabStop = false;
            // 
            // pic02
            // 
            this.pic02.Location = new System.Drawing.Point(92, 26);
            this.pic02.Name = "pic02";
            this.pic02.Size = new System.Drawing.Size(37, 40);
            this.pic02.TabIndex = 2;
            this.pic02.TabStop = false;
            // 
            // pic01
            // 
            this.pic01.Location = new System.Drawing.Point(49, 26);
            this.pic01.Name = "pic01";
            this.pic01.Size = new System.Drawing.Size(37, 40);
            this.pic01.TabIndex = 1;
            this.pic01.TabStop = false;
            // 
            // pic00
            // 
            this.pic00.Location = new System.Drawing.Point(6, 26);
            this.pic00.Name = "pic00";
            this.pic00.Size = new System.Drawing.Size(37, 40);
            this.pic00.TabIndex = 0;
            this.pic00.TabStop = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Frm_TestMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1339, 833);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbMonkField);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnEndSession);
            this.Controls.Add(this.btnEmptySelectionBackpack);
            this.Controls.Add(this.lbBackpack);
            this.Controls.Add(this.Backpack);
            this.Controls.Add(this.lblRoundCounter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSpaceInfo);
            this.Controls.Add(this.cmbMonks);
            this.Controls.Add(this.cmbResources);
            this.Controls.Add(this.cmbScrolls);
            this.Controls.Add(this.cmbDragons);
            this.Controls.Add(this.cmbPlayers);
            this.Controls.Add(this.btnMoveLeft);
            this.Controls.Add(this.btnMoveDown);
            this.Controls.Add(this.btnMoveRight);
            this.Controls.Add(this.btnMoveUp);
            this.Controls.Add(this.btnEndRound);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.btnRest);
            this.Controls.Add(this.btnGather);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtStatusPlayer);
            this.Controls.Add(this.txtMap);
            this.Name = "Frm_TestMap";
            this.Text = "TestMap";
            this.Load += new System.EventHandler(this.Frm_TestMap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBackpackMonk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSacredMonk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picActionMonk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWeaponMonk)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtMap;
        private System.Windows.Forms.RichTextBox txtStatusPlayer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGather;
        private System.Windows.Forms.Button btnRest;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Button btnEndRound;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.Button btnMoveRight;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnMoveLeft;
        private System.Windows.Forms.ComboBox cmbPlayers;
        private System.Windows.Forms.ComboBox cmbDragons;
        private System.Windows.Forms.ComboBox cmbScrolls;
        private System.Windows.Forms.ComboBox cmbResources;
        private System.Windows.Forms.ComboBox cmbMonks;
        private System.Windows.Forms.RichTextBox txtSpaceInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblRoundCounter;
        private System.Windows.Forms.Label Backpack;
        private System.Windows.Forms.ListBox lbBackpack;
        private System.Windows.Forms.Button btnEmptySelectionBackpack;
        private System.Windows.Forms.Button btnEndSession;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox picBackpackMonk;
        private System.Windows.Forms.PictureBox picSacredMonk;
        private System.Windows.Forms.PictureBox picActionMonk;
        private System.Windows.Forms.PictureBox picWeaponMonk;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ComboBox cmbMonkField;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pic48;
        private System.Windows.Forms.PictureBox pic47;
        private System.Windows.Forms.PictureBox pic46;
        private System.Windows.Forms.PictureBox pic45;
        private System.Windows.Forms.PictureBox pic44;
        private System.Windows.Forms.PictureBox pic43;
        private System.Windows.Forms.PictureBox pic42;
        private System.Windows.Forms.PictureBox pic41;
        private System.Windows.Forms.PictureBox pic40;
        private System.Windows.Forms.PictureBox pic38;
        private System.Windows.Forms.PictureBox pic37;
        private System.Windows.Forms.PictureBox pic36;
        private System.Windows.Forms.PictureBox pic35;
        private System.Windows.Forms.PictureBox pic34;
        private System.Windows.Forms.PictureBox pic33;
        private System.Windows.Forms.PictureBox pic32;
        private System.Windows.Forms.PictureBox pic31;
        private System.Windows.Forms.PictureBox pic30;
        private System.Windows.Forms.PictureBox pic28;
        private System.Windows.Forms.PictureBox pic27;
        private System.Windows.Forms.PictureBox pic26;
        private System.Windows.Forms.PictureBox pic25;
        private System.Windows.Forms.PictureBox pic24;
        private System.Windows.Forms.PictureBox pic23;
        private System.Windows.Forms.PictureBox pic22;
        private System.Windows.Forms.PictureBox pic21;
        private System.Windows.Forms.PictureBox pic20;
        private System.Windows.Forms.PictureBox pic18;
        private System.Windows.Forms.PictureBox pic17;
        private System.Windows.Forms.PictureBox pic16;
        private System.Windows.Forms.PictureBox pic15;
        private System.Windows.Forms.PictureBox pic14;
        private System.Windows.Forms.PictureBox pic13;
        private System.Windows.Forms.PictureBox pic12;
        private System.Windows.Forms.PictureBox pic11;
        private System.Windows.Forms.PictureBox pic10;
        private System.Windows.Forms.PictureBox pic08;
        private System.Windows.Forms.PictureBox pic07;
        private System.Windows.Forms.PictureBox pic06;
        private System.Windows.Forms.PictureBox pic05;
        private System.Windows.Forms.PictureBox pic04;
        private System.Windows.Forms.PictureBox pic03;
        private System.Windows.Forms.PictureBox pic02;
        private System.Windows.Forms.PictureBox pic01;
        private System.Windows.Forms.PictureBox pic00;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}