﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Fujikoro_Draft_1._1_Testing_Grounds
{
    public partial class Frm_WeaponDesigner : Form
    {
        Player weapondesignplayer = new Player("Model1");
        Resource[] weaponlist1 = new Resource[5];
        Resource[] weaponlist2 = new Resource[5];
        Resource[] weaponlist3 = new Resource[5];
        Resource[] weaponlist4 = new Resource[5];
        Resource[] weaponlist5 = new Resource[5];
        Resource[] weaponlist6 = new Resource[5];
        Resource[] weaponlist7 = new Resource[5];
        Resource[] weaponlist8 = new Resource[5];
        Resource[] weaponlist9 = new Resource[5];
        public Frm_WeaponDesigner(Player user)
        {
            InitializeComponent();
            weapondesignplayer = user;
            lbDualwielding.Text = user.Dualwielding.ToString();
        }

        private void Frm_WeaponDesigner_Load(object sender, EventArgs e)
        {

            //For Test, clear later!!
            Resource thiswood = new Resource();
            Resource thismetal = new Resource();
            Resource thisjade = new Resource();
            thiswood.Resourcetype = "Wood";
            weapondesignplayer.Backpack.Add(thiswood);
            weapondesignplayer.Backpack.Add(thiswood);
            thismetal.Resourcetype = "Metal";
            weapondesignplayer.Backpack.Add(thismetal);
            weapondesignplayer.Backpack.Add(thismetal);
            thisjade.Resourcetype = "Jade";
            weapondesignplayer.Backpack.Add(thisjade);
            weapondesignplayer.Backpack.Add(thisjade);
            UpdateBackPackListCrafting();
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    switch (j)
                    {
                        case 0:
                            weaponlist1[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 1:
                            weaponlist2[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 2:
                            weaponlist3[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 3:
                            weaponlist4[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 4:
                            weaponlist5[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 5:
                            weaponlist6[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 6:
                            weaponlist7[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 7:
                            weaponlist8[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        case 8:
                            weaponlist9[i] = weapondesignplayer.Weapon[i, j];
                            break;
                        default:
                            break;
                    }
                }
            }
            lbWeaponRow1.DataSource = weaponlist1;
            lbWeaponRow2.DataSource = weaponlist2;
            lbWeaponRow3.DataSource = weaponlist3;
            lbWeaponRow4.DataSource = weaponlist4;
            lbWeaponRow5.DataSource = weaponlist5;
            lbWeaponRow6.DataSource = weaponlist6;
            lbWeaponRow7.DataSource = weaponlist7;
            lbWeaponRow8.DataSource = weaponlist8;
            lbWeaponRow9.DataSource = weaponlist9;
            ClearSelection();
        }
        private void btnAbort_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void btnBackpackPull_Click(object sender, EventArgs e)
        {
            if (((lbWeaponRow1.SelectedIndex != -1) || (lbWeaponRow2.SelectedIndex != -1) || (lbWeaponRow3.SelectedIndex != -1) ||
                (lbWeaponRow4.SelectedIndex != -1) || (lbWeaponRow5.SelectedIndex != -1) || (lbWeaponRow6.SelectedIndex != -1) ||
                (lbWeaponRow7.SelectedIndex != -1) || (lbWeaponRow8.SelectedIndex != -1) || (lbWeaponRow9.SelectedIndex != -1)) && lbBackpack.SelectedIndex != -1)
            {
                Resource myresource = (Resource)lbBackpack.SelectedItem;
                if (myresource.Resourcetype == "Wood" && !weapondesignplayer.Dualwielding)
                {
                    if (lbWeaponRow1.SelectedIndex != -1) //Voor alle Columns toevoegen
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {

                            if (lbWeaponRow1.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);

                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist1[i];
                            }
                        }
                        lbWeaponRow1.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist1 = thisweaponlist;
                        lbWeaponRow1.DataSource = weaponlist1;
                    }
                    else if (lbWeaponRow2.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow2.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist2[i];
                            }
                        }
                        lbWeaponRow2.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist2 = thisweaponlist;
                        lbWeaponRow2.DataSource = weaponlist2;
                    }
                    else if (lbWeaponRow3.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow3.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist3[i];
                            }
                        }
                        lbWeaponRow3.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist3 = thisweaponlist;
                        lbWeaponRow3.DataSource = weaponlist3;
                    }
                    else if (lbWeaponRow4.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow4.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist4[i];
                            }
                        }
                        lbWeaponRow4.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist4 = thisweaponlist;
                        lbWeaponRow4.DataSource = weaponlist4;
                    }
                    else if (lbWeaponRow5.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow5.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist5[i];
                            }
                        }
                        lbWeaponRow5.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist5 = thisweaponlist;
                        lbWeaponRow5.DataSource = weaponlist5;
                    }
                    else if (lbWeaponRow6.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow6.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist6[i];
                            }
                        }
                        lbWeaponRow6.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist6 = thisweaponlist;
                        lbWeaponRow6.DataSource = weaponlist6;
                    }
                    else if (lbWeaponRow7.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow7.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist7[i];
                            }
                        }
                        lbWeaponRow7.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist7 = thisweaponlist;
                        lbWeaponRow7.DataSource = weaponlist7;
                    }
                    else if (lbWeaponRow8.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow8.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist8[i];
                            }
                        }
                        lbWeaponRow8.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist8 = thisweaponlist;
                        lbWeaponRow8.DataSource = weaponlist8;
                    }
                    else if (lbWeaponRow9.SelectedIndex != -1)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (int i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow9.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist9[i];
                            }
                        }
                        lbWeaponRow9.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist9 = thisweaponlist;
                        lbWeaponRow9.DataSource = weaponlist9;
                    }
                    ClearSelection();
                }
                else
                {
                    MessageBox.Show("Enkel Wood in Handle v wapen");
                }

            }
            else
            {
                MessageBox.Show("Selectie Maken in Pack en in Wapen");
            }
        }

        private void btnCraft_Click(object sender, EventArgs e)
        {
            bool permissionBase = CheckWeaponBaseLegal();
            if (permissionBase)
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                weapondesignplayer.Weapon[i, j] = weaponlist1[i];
                                break;
                            case 1:
                                weapondesignplayer.Weapon[i, j] = weaponlist2[i];
                                break;
                            case 2:
                                weapondesignplayer.Weapon[i, j] = weaponlist3[i];
                                break;
                            case 3:
                                weapondesignplayer.Weapon[i, j] = weaponlist4[i];
                                break;
                            case 4:
                                weapondesignplayer.Weapon[i, j] = weaponlist5[i];
                                break;
                            case 5:
                                weapondesignplayer.Weapon[i, j] = weaponlist6[i];
                                break;
                            case 6:
                                weapondesignplayer.Weapon[i, j] = weaponlist7[i];
                                break;
                            case 7:
                                weapondesignplayer.Weapon[i, j] = weaponlist8[i];
                                break;
                            case 8:
                                weapondesignplayer.Weapon[i, j] = weaponlist9[i];
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        private void ClearSelection()
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }
        private bool CheckWeaponBaseLegal()
        {
            // == handvat met 2 hout
            // = geen detached stukken
            // max 2 wapens
            bool weaponbaseanswer = false;

            int dualcounter = 0;
            for (int i = 0; i < 5; i++)
            {
                int weaponbasecounter = 0;
                for (int j = 0; j < 9; j++)
                {
                    switch (j)
                    {
                        case 0:
                            if (weaponlist1[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 1:
                            if ((weaponbasecounter == 1) && (weaponlist2[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }

                            else if (weaponlist2[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }
                            break;
                        case 2:
                            if ((weaponbasecounter == 1) && (weaponlist3[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }
                            else if (weaponlist3[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 3:
                            if ((weaponbasecounter == 1) && (weaponlist4[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }
                            else if (weaponlist4[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 4:
                            if ((weaponbasecounter == 1) && (weaponlist5[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }
                            else if (weaponlist5[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 5:
                            if ((weaponbasecounter == 1) && (weaponlist6[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }

                            else if (weaponlist6[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 6:
                            if ((weaponbasecounter == 1) && (weaponlist7[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }
                            else if (weaponlist7[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 7:
                            if ((weaponbasecounter == 1) && (weaponlist8[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }
                            else if (weaponlist8[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }

                            break;
                        case 8:
                            if ((weaponbasecounter == 1) && (weaponlist9[i].Resourcetype == "Wood"))
                            {
                                weaponbasecounter++;
                            }
                            else if (weaponlist9[i].Resourcetype == "Wood")
                            {
                                weaponbasecounter++;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (weaponbasecounter >= 2)
                {
                    MessageBox.Show("WeaponBaseLegal", "Nice Bro", MessageBoxButtons.OK);
                    weaponbaseanswer = true;
                    dualcounter++;
                }
            }
            if (dualcounter < 2)
            {
                weaponbaseanswer = true;

            }
            else if (dualcounter == 2)
            {
                weaponbaseanswer = true;
                weapondesignplayer.Dualwielding = true;
            }
            else
            {
                MessageBox.Show("Max 2 wapens", "ola", MessageBoxButtons.OK);
                weaponbaseanswer = false;
            }
            return weaponbaseanswer;

        }
        private void btnConfirmTotal_Click(object sender, EventArgs e)
        {
            bool permissionBase = CheckWeaponBaseLegal();
            if (permissionBase)
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                weapondesignplayer.Weapon[i, j] = weaponlist1[i];
                                UpdatepictureBoxesList1(i);
                                break;
                            case 1:
                                weapondesignplayer.Weapon[i, j] = weaponlist2[i];
                                UpdatepictureBoxesList2(i);
                                break;
                            case 2:
                                weapondesignplayer.Weapon[i, j] = weaponlist3[i];
                                UpdatepictureBoxesList3(i);
                                break;
                            case 3:
                                weapondesignplayer.Weapon[i, j] = weaponlist4[i];
                                UpdatepictureBoxesList4(i);
                                break;
                            case 4:
                                weapondesignplayer.Weapon[i, j] = weaponlist5[i];
                                UpdatepictureBoxesList5(i);
                                break;
                            case 5:
                                weapondesignplayer.Weapon[i, j] = weaponlist6[i];
                                UpdatepictureBoxesList6(i);
                                break;
                            case 6:
                                weapondesignplayer.Weapon[i, j] = weaponlist7[i];
                                UpdatepictureBoxesList7(i);
                                break;
                            case 7:
                                weapondesignplayer.Weapon[i, j] = weaponlist8[i];
                                UpdatepictureBoxesList8(i);
                                break;
                            case 8:
                                weapondesignplayer.Weapon[i, j] = weaponlist9[i];
                                UpdatepictureBoxesList9(i);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

        }

        private void btnBackpackPullBlade_Click(object sender, EventArgs e)
        {
            if (((lbWeaponRow1.SelectedIndex != -1) || (lbWeaponRow2.SelectedIndex != -1) || (lbWeaponRow3.SelectedIndex != -1) ||
               (lbWeaponRow4.SelectedIndex != -1) || (lbWeaponRow5.SelectedIndex != -1) || (lbWeaponRow6.SelectedIndex != -1) ||
               (lbWeaponRow7.SelectedIndex != -1) || (lbWeaponRow8.SelectedIndex != -1) || (lbWeaponRow9.SelectedIndex != -1)) && lbBackpack.SelectedIndex != -1)
            {
                bool answer = true;
                if (lbWeaponRow1.SelectedIndex != -1)
                {
                    int i = lbWeaponRow1.SelectedIndex;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist1[i + 1].Resourcetype == "-") && (weaponlist2[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist1[i - 1].Resourcetype == "-") && (weaponlist2[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist1[i + 1].Resourcetype == "-") && (weaponlist2[i].Resourcetype == "-") && (weaponlist1[i - 1].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {

                            if (lbWeaponRow1.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);

                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist1[i];
                            }
                        }
                        lbWeaponRow1.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist1 = thisweaponlist;
                        lbWeaponRow1.DataSource = weaponlist1;
                    }
                }
                else if (lbWeaponRow2.SelectedIndex != -1)
                {
                    answer = true;
                    int i = lbWeaponRow2.SelectedIndex;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist2[i + 1].Resourcetype == "-") && (weaponlist3[i].Resourcetype == "-") && (weaponlist1[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist2[i - 1].Resourcetype == "-") && (weaponlist3[i].Resourcetype == "-") && (weaponlist1[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist2[i + 1].Resourcetype == "-") && (weaponlist3[i].Resourcetype == "-") && (weaponlist2[i - 1].Resourcetype == "-") && (weaponlist1[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow2.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist2[i];
                            }
                        }
                        lbWeaponRow2.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist2 = thisweaponlist;
                        lbWeaponRow2.DataSource = weaponlist2;
                    }
                }
                else if (lbWeaponRow3.SelectedIndex != -1)
                {
                    bool answert = true;
                    int i = lbWeaponRow3.SelectedIndex;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist3[i + 1].Resourcetype == "-") && (weaponlist4[i].Resourcetype == "-") && (weaponlist2[i].Resourcetype == "-"))
                            {
                                answert = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist3[i - 1].Resourcetype == "-") && (weaponlist4[i].Resourcetype == "-") && (weaponlist2[i].Resourcetype == "-"))
                            {
                                answert = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist3[i + 1].Resourcetype == "-") && (weaponlist4[i].Resourcetype == "-") && (weaponlist3[i - 1].Resourcetype == "-") && (weaponlist2[i].Resourcetype == "-"))
                            {
                                answert = false;
                            }
                            break;
                    }
                    if (answert)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow3.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist3[i];
                            }
                        }
                        lbWeaponRow3.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist3 = thisweaponlist;
                        lbWeaponRow3.DataSource = weaponlist3;
                    }
                }
                else if (lbWeaponRow4.SelectedIndex != -1)
                {
                    int i = lbWeaponRow4.SelectedIndex;
                    answer = true;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist4[i + 1].Resourcetype == "-") && (weaponlist5[i].Resourcetype == "-") && (weaponlist3[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist4[i - 1].Resourcetype == "-") && (weaponlist5[i].Resourcetype == "-") && (weaponlist3[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist4[i + 1].Resourcetype == "-") && (weaponlist5[i].Resourcetype == "-") && (weaponlist4[i - 1].Resourcetype == "-") && (weaponlist3[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow4.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist4[i];
                            }
                        }
                        lbWeaponRow4.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist4 = thisweaponlist;
                        lbWeaponRow4.DataSource = weaponlist4;
                    }

                }
                else if (lbWeaponRow5.SelectedIndex != -1)
                {
                    int i = lbWeaponRow5.SelectedIndex;
                    answer = true;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist5[i + 1].Resourcetype == "-") && (weaponlist6[i].Resourcetype == "-") && (weaponlist4[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist5[i - 1].Resourcetype == "-") && (weaponlist6[i].Resourcetype == "-") && (weaponlist4[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist5[i + 1].Resourcetype == "-") && (weaponlist6[i].Resourcetype == "-") && (weaponlist5[i - 1].Resourcetype == "-") && (weaponlist4[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow5.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist5[i];
                            }
                        }
                        lbWeaponRow5.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist5 = thisweaponlist;
                        lbWeaponRow5.DataSource = weaponlist5;
                    }

                }
                else if (lbWeaponRow6.SelectedIndex != -1)
                {
                    answer = true;
                    int i = lbWeaponRow6.SelectedIndex;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist6[i + 1].Resourcetype == "-") && (weaponlist7[i].Resourcetype == "-") && (weaponlist5[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist6[i - 1].Resourcetype == "-") && (weaponlist7[i].Resourcetype == "-") && (weaponlist5[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist6[i + 1].Resourcetype == "-") && (weaponlist7[i].Resourcetype == "-") && (weaponlist6[i - 1].Resourcetype == "-") && (weaponlist5[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow6.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist6[i];
                            }
                        }
                        lbWeaponRow6.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist6 = thisweaponlist;
                        lbWeaponRow6.DataSource = weaponlist6;
                    }
                }
                else if (lbWeaponRow7.SelectedIndex != -1)
                {
                    answer = true;
                    int i = lbWeaponRow7.SelectedIndex;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist7[i + 1].Resourcetype == "-") && (weaponlist8[i].Resourcetype == "-") && (weaponlist6[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist7[i - 1].Resourcetype == "-") && (weaponlist8[i].Resourcetype == "-") && (weaponlist6[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist7[i + 1].Resourcetype == "-") && (weaponlist8[i].Resourcetype == "-") && (weaponlist7[i - 1].Resourcetype == "-") && (weaponlist6[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow7.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist7[i];
                            }
                        }
                        lbWeaponRow7.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist7 = thisweaponlist;
                        lbWeaponRow7.DataSource = weaponlist7;
                    }
                }
                else if (lbWeaponRow8.SelectedIndex != -1)
                {
                    answer = true;
                    int i = lbWeaponRow8.SelectedIndex;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist8[i + 1].Resourcetype == "-") && (weaponlist9[i].Resourcetype == "-") && (weaponlist7[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist8[i - 1].Resourcetype == "-") && (weaponlist9[i].Resourcetype == "-") && (weaponlist7[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist8[i + 1].Resourcetype == "-") && (weaponlist9[i].Resourcetype == "-") && (weaponlist8[i - 1].Resourcetype == "-") && (weaponlist7[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow8.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist8[i];
                            }
                        }
                        lbWeaponRow8.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist8 = thisweaponlist;
                        lbWeaponRow8.DataSource = weaponlist8;
                    }
                }
                else if (lbWeaponRow9.SelectedIndex != -1)
                {
                    int i = lbWeaponRow9.SelectedIndex;
                    answer = true;
                    switch (i)
                    {
                        case 0:
                            if ((weaponlist9[i + 1].Resourcetype == "-") && (weaponlist8[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        case 4:
                            if ((weaponlist9[i - 1].Resourcetype == "-") && (weaponlist8[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                        default:
                            //1-2-3
                            if ((weaponlist9[i + 1].Resourcetype == "-") && (weaponlist9[i - 1].Resourcetype == "-") && (weaponlist8[i].Resourcetype == "-"))
                            {
                                answer = false;
                            }
                            break;
                    }
                    if (answer)
                    {
                        Resource thisresource = (Resource)lbBackpack.SelectedItem;
                        Resource[] thisweaponlist = new Resource[5];

                        for (i = 0; i < 5; i++)
                        {
                            if (lbWeaponRow9.SelectedIndex == (i))
                            {
                                thisweaponlist[i] = thisresource;
                                weapondesignplayer.Backpack.Remove(thisresource);
                            }
                            else
                            {
                                thisweaponlist[i] = weaponlist9[i];
                            }
                        }
                        lbWeaponRow9.DataSource = null;
                        lbBackpack.Items.Clear();
                        UpdateBackPackListCrafting();
                        weaponlist9 = thisweaponlist;
                        lbWeaponRow9.DataSource = weaponlist9;
                    }
                }
            }
        }

        private void lbWeaponRow1_SelectedIndexChanged(object sender, EventArgs e)
        {

            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow2_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow3_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow4_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow5_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow6_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow7_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow8_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow9.SelectedIndex = -1;
        }

        private void lbWeaponRow9_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbWeaponRow1.SelectedIndex = -1;
            lbWeaponRow2.SelectedIndex = -1;
            lbWeaponRow3.SelectedIndex = -1;
            lbWeaponRow4.SelectedIndex = -1;
            lbWeaponRow5.SelectedIndex = -1;
            lbWeaponRow6.SelectedIndex = -1;
            lbWeaponRow7.SelectedIndex = -1;
            lbWeaponRow8.SelectedIndex = -1;
        }
        private void UpdatepictureBoxesList1(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 0].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList1("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList1("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList1("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList1("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList2(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 1].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList2("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList2("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList2("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList2("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList3(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 2].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList3("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList3("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList3("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList3("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList4(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 3].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList4("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList4("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList4("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList4("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList5(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 4].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList5("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList5("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList5("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList5("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList6(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 5].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList6("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList6("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList6("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList6("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList7(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 6].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList7("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList7("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList7("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList7("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList8(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 7].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList8("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList8("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList8("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList8("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void UpdatepictureBoxesList9(int i)
        {
            string answer;
            switch (weapondesignplayer.Weapon[i, 8].Resourcetype)
            {
                case "Wood":
                    SwitchPicBoxList9("Wood", i);
                    break;
                case "Metal":
                    SwitchPicBoxList9("Metal", i);
                    break;
                case "Tooth":
                    SwitchPicBoxList9("Tooth", i);
                    break;
                case "Jade":
                    SwitchPicBoxList9("Jade", i);
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList1(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic00.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic10.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic20.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic30.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic40.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList2(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic01.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic11.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic21.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic31.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic41.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList3(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic02.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic12.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic22.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic32.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic42.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList4(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic03.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic13.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic23.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic33.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic43.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList5(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic04.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic14.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic24.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic34.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic44.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList6(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic05.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic15.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic25.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic35.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic45.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList7(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic06.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic16.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic26.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic36.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic46.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList8(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic07.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic17.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic27.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic37.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic47.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        private void SwitchPicBoxList9(string x, int i)
        {
            switch (i)
            {
                case 0:
                    pic08.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 1:
                    pic18.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 2:
                    pic28.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 3:
                    pic38.Image = Image.FromFile($"{x}.jpg");
                    break;
                case 4:
                    pic48.Image = Image.FromFile($"{x}.jpg");
                    break;
                default:
                    break;
            }
        }
        //IN PLAYER CLASS ZETTEN
        private void UpdateBackPackListCrafting()
        {
            foreach (var thisresource in weapondesignplayer.Backpack)
            {
                Type mytype = thisresource.GetType();
                if (mytype.FullName == "Fujikoro_Draft_1._1_Testing_Grounds.Resource")
                {
                    lbBackpack.Items.Add(thisresource);
                }
            }
        }



    }
}
